# Generated by Django 3.1.2 on 2020-10-12 19:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0050_auto_20201011_0953'),
    ]

    operations = [
        migrations.AddField(
            model_name='item',
            name='estado_ant',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
