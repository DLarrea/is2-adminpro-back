# Generated by Django 3.1.2 on 2020-10-10 13:11

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0048_auto_20201009_1625'),
    ]

    operations = [
        migrations.AddField(
            model_name='comiteproyecto',
            name='activo',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterUniqueTogether(
            name='comiteproyecto',
            unique_together={('proyecto', 'miembro', 'activo')},
        ),
    ]
