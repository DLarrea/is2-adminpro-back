# Generated by Django 3.1.2 on 2020-10-14 02:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0052_auto_20201013_2238'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='estado',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='estado', to='api.estadoitem'),
        ),
        migrations.AlterField(
            model_name='item',
            name='estado_ant',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='estado_ant', to='api.estadoitem'),
        ),
    ]
