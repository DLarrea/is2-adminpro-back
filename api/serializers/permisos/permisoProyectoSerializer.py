from api.models import PermisoProyecto
from rest_framework import serializers

class PermisoProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = PermisoProyecto
        fields = ('id','permiso', 'permiso_nombre', 'tipo_permiso')