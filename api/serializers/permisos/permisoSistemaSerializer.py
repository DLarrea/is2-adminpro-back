from api.models import PermisoSistema
from rest_framework import serializers

class PermisoSistemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = PermisoSistema
        fields = ('id','permiso', 'permiso_nombre')