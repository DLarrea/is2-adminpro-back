from rest_framework import serializers
from api.models import RelacionItem

class RelacionItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = RelacionItem
        fields = ('id','item_relacion_1', 'item_relacion_2', 'tipo')
        depth = 1

class RelacionItemSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = RelacionItem
        fields = ('id','item_relacion_1', 'item_relacion_2', 'tipo')