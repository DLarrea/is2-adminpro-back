from api.models import RolProyectoUser
from rest_framework import serializers

class RolProyectoUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolProyectoUser
        fields = ('id','proyecto_rol', 'user')

class RolProyectoUserDepthSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolProyectoUser
        fields = ('id','rol', 'user')
        depth = 1

class RPUSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolProyectoUser
        fields = ('id','rol', 'user')