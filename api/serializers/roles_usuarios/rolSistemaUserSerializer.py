from api.models import RolSistemaUser
from rest_framework import serializers

class RolSistemaUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolSistemaUser
        fields = ('id','rol', 'user')