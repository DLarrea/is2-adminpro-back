from api.models import Solicitud, SolicitudVotacion
from rest_framework import serializers

class SolicitudSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solicitud
        fields = ('id', 'nombre', 'mensaje', 'usuario_creacion', 'lineaBase', 'proyecto', 'estado', 'item', 'fecha_creacion', 'impacto', 'items_afectados')

class SolicitudDepthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solicitud
        fields = ('id', 'nombre', 'mensaje', 'usuario_creacion', 'lineaBase', 'proyecto', 'estado', 'item', 'fecha_creacion', 'impacto', 'items_afectados')
        depth = 1

class SolicitudDepth2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Solicitud
        fields = ('id', 'nombre', 'mensaje', 'usuario_creacion', 'lineaBase', 'proyecto', 'estado', 'item', 'fecha_creacion', 'impacto', 'items_afectados')
        depth = 2

class SolicitudVotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolicitudVotacion
        fields = ('id', 'miembro', 'voto', 'solicitud')
        depth = 1

class SolicitudVotoSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = SolicitudVotacion
        fields = ('id', 'miembro', 'voto', 'solicitud')