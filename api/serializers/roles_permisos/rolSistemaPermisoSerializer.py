from rest_framework import serializers
from api.models import RolSistemaPermiso

class RolSistemaPermisoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolSistemaPermiso
        fields = ('id','rol', 'permiso')