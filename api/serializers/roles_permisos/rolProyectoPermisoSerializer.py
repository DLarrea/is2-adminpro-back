from rest_framework import serializers
from api.models import RolProyectoPermiso

class RolProyectoPermisoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolProyectoPermiso
        fields = ('id','rol', 'permiso')