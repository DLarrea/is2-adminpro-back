from api.models import ComiteProyecto
from rest_framework import serializers

class ComiteSerializer(serializers.ModelSerializer):
    class Meta:
        model = ComiteProyecto
        fields = ('id','miembro', 'proyecto', 'activo')
        depth = 1

class ComiteSerializerSimple(serializers.ModelSerializer):
    class Meta:
        model = ComiteProyecto
        fields = ('id','miembro', 'proyecto', 'activo')