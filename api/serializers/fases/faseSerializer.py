from rest_framework import serializers
from api.models import Fase

class FaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fase
        fields = ('id','nombre', 'descripcion', 'estado_fase', 'proyecto', 'orden')