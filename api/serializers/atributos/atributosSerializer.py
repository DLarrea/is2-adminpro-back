from api.models import Atributo
from rest_framework import serializers

class AtributoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Atributo
        fields = ('id','nombre', 'type')