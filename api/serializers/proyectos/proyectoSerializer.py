from rest_framework import serializers
from api.models import Proyecto

class ProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyecto
        fields = ('id','nombre', 'descripcion', 'estadoProyecto', 'gerente', 'observacion', 'fecha_inicio', 'cantidad_comite')
        depth = 2

class ProyectoDepthSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyecto
        depth = 1
        fields = ('id','nombre', 'descripcion', 'estadoProyecto', 'gerente', 'observacion', 'fecha_inicio', 'cantidad_comite')

class ProSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proyecto
        fields = ('id','nombre', 'descripcion', 'estadoProyecto', 'gerente', 'observacion', 'fecha_inicio', 'cantidad_comite')