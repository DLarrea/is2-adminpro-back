from api.models import EstadoProyecto
from rest_framework import serializers

class EstadoProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoProyecto
        fields = ('id','nombre')