from api.models import RolSistema
from rest_framework import serializers

class RolSistemaSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolSistema
        fields = ('id','nombre')