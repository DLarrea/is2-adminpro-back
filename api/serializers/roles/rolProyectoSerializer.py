from rest_framework import serializers
from api.models import RolProyecto

class RolProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = RolProyecto
        fields = ('id','nombre', 'proyecto', 'fase')