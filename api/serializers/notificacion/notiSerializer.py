from api.models import Notificacion
from rest_framework import serializers

class NotiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notificacion
        fields = ('id', 'mensaje', 'solicitud', 'proyecto', 'fecha_creacion')
