from api.models import LineaBase
from rest_framework import serializers

class LineaBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = LineaBase
        fields = ('id','nombre','descripcion','codigo','observacion','estado','fase')

class LineaBaseDepthSerializer(serializers.ModelSerializer):
    class Meta:
        model = LineaBase
        fields = ('id','nombre','descripcion','codigo','observacion','estado','fase')
        depth = 1