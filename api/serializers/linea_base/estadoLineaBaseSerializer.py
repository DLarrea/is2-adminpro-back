from api.models import EstadoLineaBase
from rest_framework import serializers

class EstadoLineaBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoLineaBase
        fields = ('id','nombre')