from api.models import EstadoItem
from rest_framework import serializers

class EstadoItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = EstadoItem
        fields = ('id','nombre')