from api.models import TipoItemFase
from rest_framework import serializers

class TipoItemFaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoItemFase
        fields = ('id','fase', 'tipoitem')

class TipoItemFaseSerializerList(serializers.ModelSerializer):
    class Meta:
        model = TipoItemFase
        fields = ('id','fase', 'tipoitem')
        depth = 1