from api.models import TipoItem
from rest_framework import serializers

class TipoItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoItem
        fields = ('id','nombre', 'descripcion', 'proyecto')