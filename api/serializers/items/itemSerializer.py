from api.models import Item
from rest_framework import serializers

class ItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        depth = 1
        fields = ('id','nombre', 'descripcion', 'version', 'prioridad', 'lineaBase', 'estado', 'tipo', 'ant', 'sig', 'current', 'values', 'estado_ant')

class ItemSerializer2(serializers.ModelSerializer):
    class Meta:
        model = Item
        depth = 2
        fields = ('id','nombre', 'descripcion', 'version', 'prioridad', 'lineaBase', 'estado', 'tipo', 'ant', 'sig', 'current', 'values', 'estado_ant')

class ItemSaveSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        fields = ('id','nombre', 'descripcion', 'version', 'prioridad', 'lineaBase', 'estado', 'tipo', 'ant', 'sig', 'current', 'values', 'estado_ant')

class ItemDrawSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        depth = 2
        fields = ('id', 'nombre', 'estado', 'lineaBase', 'prioridad', 'tipo')

class ItemsProyectoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        depth = 2
        fields = ('id', 'nombre', 'estado', 'lineaBase', 'tipo')

class ItemsRevisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Item
        depth = 2
        fields = ('id', 'nombre', 'estado', 'lineaBase', 'tipo', 'estado_ant')