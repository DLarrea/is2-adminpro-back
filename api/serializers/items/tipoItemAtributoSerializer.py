from api.models import TipoItemAtributo
from rest_framework import serializers

class TipoItemAtributoSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipoItemAtributo
        fields = ('id','tipoitem', 'atributo')