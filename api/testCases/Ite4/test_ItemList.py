import pytest
import json

from pip._vendor import requests


@pytest.mark.Ite4
def test_Item_List():
    """

    :param: query params
    :return: Lista de Items
    Esta función realiza prueba unitaria para desplegar la Lista de Items de determinada fase
    """
    fase = {
        "fase": 1,
        }
    resp = requests.get('http://localhost:8000/api/item/', data=fase)
    assert resp.status_code == 200