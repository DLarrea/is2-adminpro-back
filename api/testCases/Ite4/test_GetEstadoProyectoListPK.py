import pytest
import json

from pip._vendor import requests


@pytest.mark.Ite4
def test_Estado_Proyecto_List_PK():
    """

    :param: none
    :return: Lista de estados de los proyectos
    Esta función realiza prueba unitaria para desplegar la lista de estados de los proyectos
    """

    resp = requests.get('http://localhost:8000/api/estado-proyecto/')
    assert resp.status_code == 200