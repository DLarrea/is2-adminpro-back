import pytest
import json

from pip._vendor import requests


@pytest.mark.Ite4
def test_Item_Estado_Detail():
    """

    :param: none
    :return: Lista de estados de items
    Esta función realiza prueba unitaria para desplegar la Lista de estados de items
    """
    resp = requests.get('http://localhost:8000/api/item-estado/')
    assert resp.status_code == 200