import pytest
import json

from pip._vendor import requests


@pytest.mark.Ite4
def test_Atributos():
    """

    :param: None
    :return: Lista de atributos
    Esta función realiza prueba unitaria para desplegar la lista de atributos
    """
    resp = requests.get('http://localhost:8000/api/atributo')
    assert resp.status_code == 200