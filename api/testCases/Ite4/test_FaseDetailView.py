import pytest
import json

from pip._vendor import requests


@pytest.mark.Ite4
def test_Fase_Detail_View():
    """

    :param: ID de la Fase
    :return: ID, Nombre, Descripción, Estado, Proyecto al que pertenece y Orden de la FASE
    Esta función realiza prueba unitaria para desplegar los detalles de una fase
    """
    resp = requests.get('http://localhost:8000/api/fase/1')
    assert resp.status_code == 200