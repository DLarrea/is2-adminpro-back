import pytest
import json

from pip._vendor import requests


@pytest.mark.Ite4
def test_Linea_Base_Estado_List():
    """

    :param: none
    :return: Lista de estados de línea base
    Esta función realiza prueba unitaria para desplegar la lista de Estado de línea base
    """

    resp = requests.get('http://localhost:8000/api/linea-base/estados')
    assert resp.status_code == 200