import pytest
import json

from pip._vendor import requests

@pytest.mark.Ite4
def test_Get_Item_Historial():
    """

    :param: Item ID
    :return: Historal de Item
    Esta función realiza prueba unitaria para desplegar el Historal de Item
    """
    resp = requests.get('http://localhost:8000/api/item/1/historial')
    assert resp.status_code == 200,"Historial obtenido"