import pytest
import json

from pip._vendor import requests

@pytest.mark.Ite4
def test_Estado_Proyecto_List():
    """

    :param: ID de proyecto
    :return: Detalles del estado del proyecto
    Esta función realiza prueba unitaria para desplegar los detalles del estado del proyecto
    """
    resp = requests.get('http://localhost:8000/api/estado-proyecto/1')
    assert resp.status_code == 200