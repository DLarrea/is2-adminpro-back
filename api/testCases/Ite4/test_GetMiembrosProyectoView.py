import pytest
import requests
import json

@pytest.mark.Ite4
def test_Get_Miembros_Proyectos():
    """

    :param: ID Proyecto
    :return: Lista de miembros de proyecto
    Esta función realiza prueba unitaria para desplegar la lista de miembros del proyecto
    """
    resp = requests.get('http://localhost:8000/api/proyecto/1/users')
    assert resp.status_code == 200,"Miembros obtenidos"