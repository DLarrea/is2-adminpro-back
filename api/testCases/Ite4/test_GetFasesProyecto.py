import pytest
import json

from pip._vendor import requests

@pytest.mark.Ite4
def test_Get_Fases_Proyecto():
    """

    :param: query params
    :return: Fases del proyecto
    Esta función realiza prueba unitaria para desplegar las Fases del proyecto
    """
    data = {
        "proyecto": 1
    }
    resp = requests.get('http://localhost:8000/api/fase', data=data)
    assert resp.status_code == 200