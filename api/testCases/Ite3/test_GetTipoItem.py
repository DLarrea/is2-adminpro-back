import pytest
import requests
import json

@pytest.mark.ite3
def test_get_tipo_item():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre obtener tipo de ítem
    """
    tipoItem = {
        "nombre": "Prueba",
        "descripcion": "Unit Test"
        }
    resp = requests.get('http://localhost:8000/api/tipoItem/', data=tipoItem)
    assert resp.status_code == 200