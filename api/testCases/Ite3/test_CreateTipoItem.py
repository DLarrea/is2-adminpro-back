import pytest
import requests
import json

@pytest.mark.ite3
def test_create_tipo_item():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre crear tipo de ítem
    """
    tipoItem = {
        "nombre": "Prueba",
        "descripcion": "Unit Test",
        "fase": 1
        }
    resp = requests.post('http://localhost:8000/api/tipoItem/', data=tipoItem)
    assert resp.status_code == 201