import pytest
import requests
import json

@pytest.mark.ite3
def test_create_atributo():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre crear un atributo
    """
    atributo= {
        "nombre": "Prueba",
        "descripcion": "Unit Test",
        "type": "T"
        }
    resp = requests.post('http://localhost:8000/api/atributo/', data=atributo)
    assert resp.status_code == 201