import pytest
import requests
import json

@pytest.mark.ite3
def test_get_atributo():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre obtener un atributo
    """
    atributo = {
        "nombre": "Prueba",
        "descripcion": "Unit Test",
        "type": "T"
        }
    resp = requests.get('http://localhost:8000/api/atributo/')
    assert resp.status_code == 200