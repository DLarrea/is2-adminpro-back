import pytest
import requests

@pytest.mark.ite3
def test_fases():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre obtener una fase
    """
    queryParams = {"proyecto":1}
    resp = requests.get('http://localhost:8000/api/fase/', params=queryParams)
    assert resp.status_code == 200, "Error al obtener fase"