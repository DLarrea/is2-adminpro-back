import pytest
import requests
import json

@pytest.mark.ite3
def test_create_proyecto():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre crear un proyecto
    """
    proyecto = {
        "nombre": "Prueba",
        "descripcion": "Unit Test",
        "estadoProyecto": 1
        }
    resp = requests.post('http://localhost:8000/api/proyecto/', data=proyecto)
    assert resp.status_code == 201