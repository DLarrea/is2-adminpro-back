import pytest
import requests
import json

@pytest.mark.ite3
def test_get_proyecto():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre obtener un proyecto
    """
    proyecto = {
        "nombre": "Prueba",
        "descripcion": "Unit Test",
        "estadoProyecto": 1
        }
    resp = requests.get('http://localhost:8000/api/proyecto/', data=proyecto)
    assert resp.status_code == 200,"Fases obtenidas"