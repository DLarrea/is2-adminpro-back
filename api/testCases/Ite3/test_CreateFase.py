import pytest
import requests
import json

@pytest.mark.ite3
def test_create_fase():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre crear una fase
    """
    fase = {
        "nombre": "Prueba",
        "descripcion": "Unit Test",
        "estado": "True",
        "proyecto": 1
        }
    resp = requests.post('http://localhost:8000/api/fase/', data=fase)
    assert resp.status_code == 201