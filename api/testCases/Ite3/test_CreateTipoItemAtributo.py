import pytest
import requests
import json

@pytest.mark.ite3
def test_create_tipo_item_atributo():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre crear atributo de tipo de ítem
    """
    TipoItemAtributo = {
        "tipoitem": 1,
        "atributo": 1
        }
    resp = requests.post('http://localhost:8000/api/tipoItemAtributo/', data=TipoItemAtributo)
    assert resp.status_code == 201