import pytest
import requests
import json

@pytest.mark.ite3
def test_get_tipo_item_atributo():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre obtener atributo de tipo de ítem
    """
    TipoItemAtributo = {
        "tipoitem": 1,
        "atributo": 1
        }
    resp = requests.get('http://localhost:8000/api/tipoItemAtributo/')
    assert resp.status_code == 200