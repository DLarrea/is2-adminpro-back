import pytest
import requests
import json

@pytest.mark.login
def test_login():
    """

    :return: nada
    Esta función realiza prueba unitaria sobre el login
    """
    usuario = {
        "username": "osmar@gmail.com",
        "password": "osmar",
    }
    u_json = json.dumps(usuario)
    resp = requests.post('http://localhost:8000/api/login/', u_json)
    assert resp.status_code == 201

