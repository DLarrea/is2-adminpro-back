import pytest
import requests

@pytest.mark.Ite5
def test_crearRelacionesItems():
    """

    :param: ItemRelacion
    :return: None
    Prueba unitaria para crear una relacion entre 2 items
    """

    relacion = {
        "item_relacion_1": 61,
        "item_relacion_2": 45
    }

    resp = requests.post(f'http://localhost:8000/api/relacionar-item/', relacion)

    if resp.status_code == 500:
        assert resp.status_code == 201, pytest.fail("No se puede crear la relación entre ambos items", False)
    else:
        assert resp.status_code == 201, pytest.fail(resp.json()["mensaje"], False)
    