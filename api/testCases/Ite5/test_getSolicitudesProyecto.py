import pytest
import requests

@pytest.mark.Ite5
def test_getSolicitudesProyecto():
    """

    :param: ID Proyecto
    :return: None
    Prueba unitaria para obtener solicitudes por proyecto
    """
    id_proyecto = 8

    resp = requests.get(f'http://localhost:8000/api/solicitud/?proyecto={id_proyecto}')
    
    assert resp.status_code == 200, pytest.fail("ID Proyecto no existe", False)