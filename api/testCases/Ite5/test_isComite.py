import pytest
import requests

@pytest.mark.Ite5
def test_isComite():
    """

    :param: ID Usuario
    :param: ID Proyecto 
    :return: None
    Prueba unitaria para saber si un usuario es miembro del comite de un proyecto
    """
    id_usuario = 4
    id_proyecto = 8

    resp = requests.get(f'http://localhost:8000/api/iscomite/{id_usuario}?proyecto={id_proyecto}')

    if resp.status_code == 500:
        assert resp.status_code == 201, pytest.fail("Error", False)
    else:
        assert resp.status_code == 200 and resp.json()["miembro"], pytest.fail("El usuario no es miembro del comite del proyecto", False)
    