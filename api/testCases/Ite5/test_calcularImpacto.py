import pytest
import requests

@pytest.mark.Ite5
def test_calcularImpacto():
    """

    :param: ID Item
    :return: None
    Prueba unitaria para obtener el valor del impacto de la posible modificacion del item
    """
    id_item = 1

    resp = requests.get(f'http://localhost:8000/api/item-impacto/{id_item}')
    
    assert resp.status_code == 200