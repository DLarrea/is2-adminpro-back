import pytest
import requests

@pytest.mark.Ite5
def test_cerrarFase():
    """

    :param: Fase
    :return: None
    Prueba unitaria para cerrar una fase
    """
    fase = {
        "id": 8,
        "nombre": "Fase 1",
        "descripcion": "Orden 1",
        "proyecto": 14,
        "estado_fase": "CE",
        "orden": 1
    }

    resp = requests.put(f'http://localhost:8000/api/fase/{fase["id"]}', fase)
    
    assert resp.status_code == 200, pytest.fail("La fase posee items que no estan aprobados o que no estan en una linea base", False)    