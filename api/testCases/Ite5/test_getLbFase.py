import pytest
import requests

@pytest.mark.Ite5
def test_getLbFase():
    """

    :param: ID Fase
    :return: None
    Prueba unitaria para obtener lineas base por fase
    """
    id_fase = 8

    resp = requests.get(f'http://localhost:8000/api/linea-base/?fase={id_fase}')
    
    assert resp.status_code == 200, pytest.fail("ID Fase no existe", False)