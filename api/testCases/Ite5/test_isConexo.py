import pytest
import requests

@pytest.mark.Ite5
def test_isConexo():
    """

    :param: ID Proyecto
    :return: None
    Prueba unitaria para saber si un el grafo de relaciones de items de un proyecto es conexo
    """
    id_proyecto = 8

    resp = requests.get(f'http://localhost:8000/api/relacionar-item/{id_proyecto}')

    if resp.status_code == 500:
        assert resp.status_code == 201, pytest.fail("Error", False)
    else:
        assert resp.status_code == 200 and resp.json()["estado"] == 0, pytest.fail("El grafo del proyecto es disconexo", False)