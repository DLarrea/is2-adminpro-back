import pytest
import requests
import json

@pytest.mark.create_user
def test_create_user():
    """

    :return: nada
    Esta función realiza pruebas unitarias sobre creación de usuario
    """
    usuario = {
        "username": "user_testing2@gmail.com",
        "password": "user_testing2",
        "email": "user_testing2@usuario.com",
        "first_name": "user2",
        "last_name": "test2"
    }
    resp = requests.post('http://localhost:8000/api/user/', data=usuario)
    assert resp.status_code == 201

