class IdsManager():
    def ids_raw_rol(roles_ids):
        roles_ids_raw = []
        for rol in roles_ids:
            roles_ids_raw.append(rol["rol_id"])
        return roles_ids_raw

    def ids_raw_permiso(permisos_ids):
        permisos_ids_raw = []
        for permiso in permisos_ids:
            permisos_ids_raw.append(permiso["permiso_id"])
        return permisos_ids_raw

    def ids_raw_atributo(atributos_ids):
        atributos_ids_raw = []
        for a in atributos_ids:
            atributos_ids_raw.append(a["atributo_id"])
        return atributos_ids_raw