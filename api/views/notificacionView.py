from rest_framework import generics
from api.models import Proyecto, Notificacion
from api.serializers.notificacion.notiSerializer import NotiSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


import json

class notificacionList(APIView):
    """

    """

    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP
        Esta función sirve para obtener las notificaciones de linea base
        """
        proyecto_id = request.GET.get("proyecto")
        notificaciones = Notificacion.objects.filter(proyecto_id=proyecto_id)
        serializer = NotiSerializer(notificaciones, many=True)
        return Response(serializer.data)



    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito o de error
        Esta función crea una notificacion y la serializa
        """
        serializer = NotiSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NotificacionDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Notificacion.objects.all()
    serializer_class = NotiSerializer