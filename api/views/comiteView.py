from rest_framework import generics
from api.models import ComiteProyecto, User, RolProyecto, RolProyectoUser, Proyecto
from api.serializers.comite.comiteSerializer import ComiteSerializer, ComiteSerializerSimple
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import json

class ComiteList(APIView):
    def get(self, request, format=None):
        """

        :param request: parámetro HTTP
        :param format:
        :return: una rspuesta serializada de los proyectos para el Comité

        Esta función obtiene una lista de miembros de Comité de un proyecto
        """
        proyecto_id = request.GET.get("proyecto")
        miembros = ComiteProyecto.objects.filter(proyecto=proyecto_id, activo=True)
        serializer = ComiteSerializer(miembros, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """

        :param request: petición HTTP
        :param format:
        :return: una respuesta HTTP con estado 201 de creado o de error

        Esta función hace un post de la lista de Comité
        """
        proyecto_id = request.data['proyecto']
        proyecto = Proyecto.objects.get(pk=proyecto_id)
        miembros_comite = ComiteProyecto.objects.filter(proyecto=proyecto_id, activo=True).count()

        if miembros_comite <= (proyecto.cantidad_comite-1):
            serializer = ComiteSerializerSimple(data=request.data)
            if serializer.is_valid():
                serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        elif miembros_comite == proyecto.cantidad_comite:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        return Response(status=status.HTTP_400_BAD_REQUEST)


class ComiteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = ComiteProyecto.objects.all()
    serializer_class = ComiteSerializer

class IsComite(APIView):
    def get(self, request, pk, format=None):
        """

        :param request: petición HTTP
        :param pk: clave primaria de usuario
        :param queryparam proyecto: clave primaria del proyecto
        :param format:
        :return: una respuesta HTTP que indica si el usuario es miembro de un proyecto

        Esta función verificar si un usuario es miembro de un proyecto
        """
        proyecto_id = request.GET.get("proyecto")
        is_miembro = ComiteProyecto.objects.filter(proyecto=proyecto_id, activo=True, miembro=pk).exists()
        if is_miembro:
            return Response({"miembro": True})
        else: 
            return Response({"miembro": False})