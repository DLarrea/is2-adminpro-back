from rest_framework import generics
from api.models import Proyecto, ComiteProyecto, RolProyecto, RolProyectoUser, LineaBase, SolicitudVotacion
from api.models import Solicitud, Notificacion, EstadoLineaBase, EstadoItem, Item, RelacionItem
from api.serializers.solicitud.solicitudSerializer import SolicitudSerializer, SolicitudDepthSerializer, \
    SolicitudVotoSerializer, SolicitudDepth2Serializer, SolicitudVotoSimpleSerializer
from api.serializers.comite.comiteSerializer import ComiteSerializer
from api.serializers.items.itemSerializer import ItemSerializer, ItemDrawSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json


class solicitudList(APIView):

    calc_impacto = 0
    item_recorridos = ""

    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP
        Esta función obtiene las notificaciones de linea base
        """
        proyecto_id = request.GET.get("proyecto")

        proyecto = Proyecto.objects.get(pk=proyecto_id)

        miembros = ComiteProyecto.objects.filter(proyecto=proyecto_id)

        solicitudes = Solicitud.objects.filter(proyecto_id=proyecto_id).order_by('estado', '-fecha_creacion')

        solicitudes_list = []
        for n in solicitudes:
            lista = []
            items_afectados = []
            votos = SolicitudVotacion.objects.filter(solicitud=n.id)
            if n.items_afectados != '':
                lista = list(map(int, n.items_afectados.split("-")))
                items_afectados = Item.objects.filter(id__in=lista)
            else: 
                lista = []
            
            solicitudes_list.append(
                {
                    "total": proyecto.cantidad_comite,
                    "votos_usuarios": SolicitudVotoSerializer(votos, many=True).data,
                    "votos_cantidad": votos.count(),
                    "solicitud": SolicitudDepth2Serializer(n).data,
                    "items_afectados": ItemDrawSerializer(items_afectados, many=True).data
                }
            )
        
        response = {
            "miembros": ComiteSerializer(miembros, many=True).data,
            "datos": solicitudes_list
        }
        print(response)
        return Response(response)

    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP de éxito o de error
        Esta función crea una notificacion y la serializa
        """

        item = Item.objects.get(id=request.data['item'])
        lineabase = LineaBase.objects.get(id=request.data['lineaBase'])
        mensaje = "SRLB_{0}-Item-{1}".format(lineabase.nombre, item.nombre)

        solicitudList.calc_impacto = 0
        solicitudList.item_recorridos = "{0}".format(item.id)
        self.impacto(item)
        #items_afectado = Item.objects.filter(id__in=solicitudList.item_recorridos)
        solicitud = {
            'nombre': mensaje,
            'mensaje': request.data['mensaje'],
            'usuario_creacion': request.data['usuario_creacion'],
            'lineaBase': request.data['lineaBase'],
            'proyecto': request.data['proyecto'],
            'item': request.data['item'],
            'impacto': solicitudList.calc_impacto,
            "items_afectados": solicitudList.item_recorridos
        }
        serializer = SolicitudSerializer(data=solicitud)
        if serializer.is_valid():
            serializer.save()
            estado = EstadoLineaBase.objects.get(nombre='RE')
            lineabase.estado = estado
            lineabase.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def impacto(self, item):
        """

        :param request: recibe un Item
        :return: none
        Esta función calcula el impacto
        """
        if item.current:
            relacionesA = RelacionItem.objects.filter(item_relacion_1=item)
            for i in relacionesA:
                lista = list(map(int, solicitudList.item_recorridos.split("-")))
                if (not (i.item_relacion_2.id in lista)) and i.item_relacion_2.current:
                    solicitudList.item_recorridos = solicitudList.item_recorridos + "-{0}".format(i.item_relacion_2.id)
                    solicitudList.calc_impacto = solicitudList.calc_impacto + i.item_relacion_2.prioridad
                    self.impacto(i.item_relacion_2)

class solicitudDetail(APIView):
    def get_object(self, pk):
        """
        :param pk: clave primaria de Solicitud
        :return: Solicitud de acuerdo a su clave primaria
        Esta función obtiene una solicitud
        """
        try:
            return Solicitud.objects.get(pk=pk)
        except Solicitud.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        """

        :param request: Miembro y Voto
        :param pk: ID de Notificacion Linea Base
        :param format: query param
        :return: HTTP 200
        Esta funcion realiza el voto para romper una linea base
        """

        solicitud = self.get_object(pk)
        proyecto = solicitud.proyecto

        miembros_activos = ComiteProyecto.objects.filter(proyecto=proyecto.id, activo=True)

        print(request.data)
        voto_serializer = SolicitudVotoSimpleSerializer(data=request.data)

        votante = ComiteProyecto.objects.filter(pk=request.data["miembro"]).first()

        # is_active = ComiteProyecto.objects.filter(proyecto=proyecto.id, activo=True, miembro=votante.miembro).exists()
        
        if voto_serializer.is_valid() and votante.activo:

            has_voto = SolicitudVotacion.objects.filter(solicitud=pk, miembro=request.data["miembro"]).exists()
            if not has_voto:
                voto_serializer.save()

            votos_total = SolicitudVotacion.objects.filter(solicitud=pk, miembro__in=miembros_activos)

            if (miembros_activos.count() == proyecto.cantidad_comite) and proyecto.cantidad_comite == votos_total.count():

                votos = []
                for i in votos_total:
                    votos.append(i.voto)

                noti = Notificacion()

                if (self.votacion(votos)):
                    solicitud.estado = 'SAP'
                    noti.mensaje = 'La solicitud: ' + solicitud.nombre + ' : fue aprobada'
                    self.romperlineabase(solicitud.lineaBase, solicitud.item)
                else:
                    solicitud.estado = 'SRE'
                    noti.mensaje = 'La solicitud: ' + solicitud.nombre + ' : fue rechazada'

                noti.solicitud = solicitud
                noti.proyecto = solicitud.proyecto
                noti.save()

                solicitud.save()
            return Response(status.HTTP_200_OK)

    def votacion(self, votos):
        """

        :param request: recibe votos
        :return: voto
        Esta función cuenta la cantidad de votos
        """
        for voto in set(votos):
            if votos.count(voto) > len(votos) // 2:
                return voto
        return None

    def romperlineabase(self, lineabase, item):
        """

        :param request: lineabase,item
        :return: none
        Esta función rompe la linea base
        """
        estado_lb = EstadoLineaBase.objects.get(nombre='RO')
        lineabase.estado = estado_lb
        estado_itemDE = EstadoItem.objects.get(nombre='DE')
        estado_itemRE = EstadoItem.objects.get(nombre='RE')
        item.estado = estado_itemDE
        lineabase.save()
        item.save()
        items_lineabase = Item.objects.filter(lineaBase=lineabase)
        for i in items_lineabase:
            i.lineaBase = None
            i.save()
        self.recorrido(item, estado_itemRE)

    def recorrido(self, item, estado):
        """

        :param request: item, estado
        :return: none
        Esta función realiza el recorrido
        """
        relacionesA = RelacionItem.objects.filter(item_relacion_1=item)
        for i in relacionesA:
            if i.item_relacion_2.estado_ant is None:
                print("estado anterior no RE", i.item_relacion_2.estado_ant)
                i.item_relacion_2.estado_ant = i.item_relacion_2.estado
            i.item_relacion_2.estado = estado
            i.item_relacion_2.save()
            self.recorrido(i.item_relacion_2, estado)