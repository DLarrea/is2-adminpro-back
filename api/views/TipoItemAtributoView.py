from rest_framework import generics
from api.models import TipoItemAtributo
from api.serializers.items.tipoItemAtributoSerializer import TipoItemAtributoSerializer


class TipoItemAtributoList(generics.ListCreateAPIView):
    queryset = TipoItemAtributo.objects.all()
    serializer_class = TipoItemAtributoSerializer


class TipoItemAtributoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TipoItemAtributo.objects.all()
    serializer_class = TipoItemAtributoSerializer