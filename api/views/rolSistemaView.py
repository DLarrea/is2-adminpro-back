from api.models import RolSistema, PermisoSistema, RolSistemaPermiso, RolSistemaUser
from api.serializers.roles.rolSistemaSerializer import RolSistemaSerializer
from api.serializers.permisos.permisoSistemaSerializer import PermisoSistemaSerializer
from api.serializers.roles_permisos.rolSistemaPermisoSerializer import RolSistemaPermisoSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.shortcuts import get_object_or_404, get_list_or_404
from django.db import connection

import json

class getPostView(APIView):

    def get(self, request, format=None):
        """

        :param request: Petición HTTP GET
        :param format: None
        :return: Respuesta HTTP
        Lista de roles de sistema
        """
        roles = RolSistema.objects.all()
        serializer = RolSistemaSerializer(roles, many=True)
        return Response(serializer.data)


    def post(self, request, format=None):
        """

        :param request: Petición HTTP POST, body RolSistema
        :param format:
        :return: Respuesta HTTP
        Rol de sistema creado
        """
        serializer = RolSistemaSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class getPutDeleteView(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        """

        :param pk: Id del Rol Sistema
        :return: Rol Sistema
        """
        try:
            return RolSistema.objects.get(pk=pk)
        except RolSistema.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """

        :param request: Petición HTTP GET
        :param pk: Id Rol Sistema
        :param format:
        :return: Rol Sistema
        """
        rol = self.get_object(pk)
        serializer = RolSistemaSerializer(rol)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """

        :param request: Petición HTTP PUT
        :param pk: Id Rol Sistema
        :param format:
        :return: Rol Sistema
        """
        rol = self.get_object(pk)
        serializer = RolSistemaSerializer(rol, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """

        :param request: Petición HTTP DELETE
        :param pk: id Rol Sistema
        :param format:
        :return: Respuesta HTTP
        """
        rol = self.get_object(pk)
        rol_permisos = RolSistemaPermiso.objects.filter(rol_id=pk)
        print(rol_permisos)
        if rol_permisos.count() != 0:
            return Response({"error": True, "mensaje": "Existen permisos asociados"}, status=status.HTTP_400_BAD_REQUEST)
        rol_user = RolSistemaUser.objects.filter(rol_id=pk)
        if rol_user.count() != 0:
            return Response({"error": True, "mensaje": "Existen usuarios asociados"}, status=status.HTTP_400_BAD_REQUEST)
        rol.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class asignarEliminarPermisoView(APIView):
    def post(self, request, rk, pk):
        """

        :param request: Petición HTTP POST
        :param rk: Id Rol Sistema
        :param pk: id Permiso Sistema
        :return: Rol Sistema Permiso
        """
        rol = RolSistema.objects.get(pk=rk)
        permiso = PermisoSistema.objects.get(pk=pk)
        rol_permiso = RolSistemaPermiso.objects.create(rol_id= rol.id, permiso_id=permiso.id)
        rol_permiso.save()
        s_rol_permiso = RolSistemaPermisoSerializer(rol_permiso)
        return Response(s_rol_permiso.data)

    def delete(self, request, rk, pk):
        """

        :param request: Petición HTTP DELETE
        :param rk: Id Rol Sistema
        :param pk: Id Permiso Sistema
        :return: Respuesta HTTP
        """
        rol_permiso = RolSistemaPermiso.objects.get(rol_id=rk, permiso_id=pk)
        rol_permiso.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class obtenerPermisosView(APIView):
    def get(self, request, pk):
        """

        :param request:
        :param pk: id Rol Sistema
        :return: Permisos Sistema asignados y disponibles
        """
        rol = RolSistema.objects.get(pk=pk)
        s_rol = RolSistemaSerializer(rol)
        roles_permisos = RolSistemaPermiso.objects.filter(rol_id=pk)
        ids = []
        for rp in roles_permisos:
            ids.append(rp.permiso_id)
        permisos_asignados = PermisoSistema.objects.filter(id__in=ids)
        s_permisos_asignados = PermisoSistemaSerializer(permisos_asignados, many=True)
        permisos_disponibles = PermisoSistema.objects.all().exclude(id__in=ids)
        s_permisos_disponibles = PermisoSistemaSerializer(permisos_disponibles, many=True)

        response = {
            "rol": s_rol.data,
            "permisos_disponibles": s_permisos_disponibles.data,
            "permisos_asignados": s_permisos_asignados.data
        }
        return Response(response)
