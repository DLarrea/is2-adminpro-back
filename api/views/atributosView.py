from rest_framework import generics
from api.models import Atributo
from api.serializers.atributos.atributosSerializer import AtributoSerializer


class AtributoList(generics.ListCreateAPIView):
    queryset = Atributo.objects.all()
    serializer_class = AtributoSerializer


class AtributoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Atributo.objects.all()
    serializer_class = AtributoSerializer