from rest_framework.decorators import api_view
from api.models import RolSistema, RolSistemaUser, RolProyectoUser, RolProyecto, RolSistemaPermiso, RolProyectoPermiso, PermisoSistema, PermisoProyecto, ComiteProyecto
from rest_framework.response import Response
import json
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from api.serializers.usuarios.userSerializer import UserSerializer
from api.serializers.roles.rolSistemaSerializer import RolSistemaSerializer
from api.serializers.roles.rolProyectoSerializer import RolProyectoSerializer
from api.serializers.roles_usuarios.rolProyectoUserSerializer import RolProyectoUserDepthSerializer
from api.serializers.permisos.permisoProyectoSerializer import PermisoProyectoSerializer
from api.serializers.permisos.permisoSistemaSerializer import PermisoSistemaSerializer
from rest_framework import status
from rest_framework.views import APIView
import pyrebase
from api.utils.ids import IdsManager

config = {
    "apiKey": "AIzaSyDKJBlAy_TFarWFA4GsD-FC42ZqAzYHvuY",
    "authDomain": "is2-admin-pro.firebaseapp.com",
    "databaseURL": "https://is2-admin-pro.firebaseio.com",
    "projectId": "is2-admin-pro",
    "storageBucket": "is2-admin-pro.appspot.com",
    "messagingSenderId": "106217067862",
    "appId": "1:106217067862:web:71528e0bafb8e49b89f2f7"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()


class getPostView(APIView):

    def get(self, request, format=None):
        """
        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función obtiene la lista de todos los usuarios
        """
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        """
        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito (creado) o de error
        Esta función crea un nuevo usuario con sus datos y le asigna un rol de usuario
        """
        roles = request.data.get("roles", None)
        serializer = UserSerializer(data=request.data.get("user"))
        if serializer.is_valid():
            if serializer.data.get('email', None) is None or \
                    serializer.data.get('email') == "" or \
                    serializer.data.get('password', None) is None or \
                    serializer.data.get('password') == "" or \
                    serializer.data.get('username', None) is None or \
                    serializer.data.get('username') == "" or \
                    serializer.data.get('first_name', None) is None or \
                    serializer.data.get('first_name') == "" or \
                    serializer.data.get('last_name', None) is None or \
                    serializer.data.get('last_name') == "":
                response = {
                    "error": True,
                    "mensaje":"Datos incompletos"
                }
                return Response(response, status=status.HTTP_400_BAD_REQUEST)

            try:
                user = auth.create_user_with_email_and_password(serializer.data.get('email'), serializer.data.get('password', None))
                user_local, created = User.objects.get_or_create(username=serializer.data.get('username'),
                                                               email=serializer.data.get('email'),
                                                               first_name=serializer.data.get('first_name'),
                                                               last_name=serializer.data.get('last_name'))
                user_local.set_password(serializer.data.get('password'))
                user_local.save()
                userResponse = User.objects.get(username=serializer.data.get("username"))
                serializerResponse = UserSerializer(userResponse)

                #Asignar roles
                for rol in roles:
                    print(rol["id"])
                    rol_user = RolSistemaUser.objects.create(rol_id=rol["id"], user_id=serializerResponse.data.get("id"))
                    rol_user.save()
                return Response(serializerResponse.data, status=status.HTTP_201_CREATED)
            except:
                response = {
                    "error":True,
                    "mensaje":"Error al crear usuario"
                }
                return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class getPutDeleteView(APIView):

    def put(self, request, pk, format=None):
        """
        :param request: recibe una petición HTTP
        :param pk: clave primaria de usuario
        :param format:
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función verifica el estado de un usuario
        """
        user = User.objects.get(pk=pk)
        serializer = UserSerializer(request.data)
        user.is_active = serializer.data.get("is_active")
        user.save()
        return Response(status=status.HTTP_200_OK)

class getUserRolSistema(APIView):

    def get(self, request, uk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :return: una respuesta HTTP
        Esta función obtiene la lista de roles del sistema de un usuario
        """
        roles_asignados = RolSistemaUser.objects.all()
        roles_asignados_user = roles_asignados.filter(user_id=uk)
        ids = []
        for ra in roles_asignados_user:
            ids.append(ra.rol_id)
        roles = RolSistema.objects.filter(id__in=ids)
        s_roles = RolSistemaSerializer(roles, many=True)
        return Response(s_roles.data,status=status.HTTP_200_OK)

class getUserRolProyecto(APIView):

    def get(self, request, uk, pk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de proyecto
        :param pk: clave primaria de usuario
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función lista los roles de proyecto de un usuario
        """
        roles_proyecto = RolProyecto.objects.filter(proyecto_id=pk)
        user_roles = RolProyectoUser.objects.filter(user_id=uk)

        res = []
        for rp in roles_proyecto:
            isIn = False
            for ru in user_roles:
                if rp.id == ru.rol_id:
                    isIn = True
                    break
            if isIn:
               res.append(rp)
        s_user_roles = RolProyectoSerializer(res, many=True)

        return Response(s_user_roles.data,status=status.HTTP_200_OK)

class asignarEliminarRolSistema(APIView):

    def post(self, request, uk, rk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param rk: clave primaria de rol
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función crea un rol de sistema para un usuario
        """
        user = get_object_or_404(User, pk=uk)
        rol = get_object_or_404(RolSistema, pk=rk)

        rol_user = RolSistemaUser.objects.create(rol_id=rol.id, user_id=user.id)
        rol_user.save()

        return Response({"mensaje": "Rol asignado"}, status=status.HTTP_200_OK)

    def delete(self, request, uk, rk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param rk: clave primaria de rol de sistema
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función elimina un rol de sistema de un usuario
        """
        user = get_object_or_404(User, pk=uk)
        rol = get_object_or_404(RolSistema, pk=rk)

        rol_user = RolSistemaUser.objects.get(rol_id=rol.id, user_id=user.id)
        rol_user.delete()

        return Response({"mensaje": "Rol eliminado"}, status=status.HTTP_200_OK)

class asignarEliminarRolProyecto(APIView):

    def post(self, request, uk, rk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param rk: clave primara de rol de proyecto
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función crea un rol de proyecto para un usuario
        """
        user = get_object_or_404(User, pk=uk)
        rol = get_object_or_404(RolProyecto, pk=rk)

        rol_user = RolProyectoUser.objects.create(rol_id=rol.id, user_id=user.id)
        rol_user.save()

        return Response({"mensaje": "Rol asignado"}, status=status.HTTP_200_OK)

    def delete(self, request, uk, rk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param rk: clave primaria de rol de proyecto
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función elimina un rol de proyecto de un usuario
        """

        rol_user = RolProyectoUser.objects.get(rol_id=rk, user_id=uk)
        rol_user.delete()

        return Response({"mensaje": "Rol desasignado"}, status=status.HTTP_200_OK)

class obtenerPermisosSistema(APIView):

    def get(self, request, uk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de roles de sistema
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función obtiene los roles de sistema
        """
        permisos= self.permisosRolesSistema(uk)
        return Response(permisos, status=status.HTTP_200_OK)

    def permisosRolesSistema(self, uk):
        """
        :param uk: clave primaria de usuario
        :return: una lista con los datos de permiso
        Esta función obtiene los permisos de roles del sistema
        """
        response = []
        roles_ids = RolSistemaUser.objects.filter(user_id=uk).values('rol_id')
        roles_ids_raw = IdsManager.ids_raw_rol(roles_ids)
        for id in roles_ids_raw:
            permisos_ids = RolSistemaPermiso.objects.filter(rol_id=id).values('permiso_id')
            permisos_ids_raw = IdsManager.ids_raw_permiso(permisos_ids)
            permisos = PermisoSistema.objects.filter(id__in=permisos_ids_raw)
            for p in permisos:
                isIn = False
                for r in response:
                    if p.id == r["id"]:
                        isIn = True
                if not isIn:
                    response.append({
                        "id": p.id,
                        "permiso": p.permiso,
                        "permiso_nombre": p.permiso_nombre
                    })
        return response

class obtenerPermisosProyecto(APIView):

    def get(self, request, uk, pk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param pk: clave primaria de permisos de roles de proyecto
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función obtiene los permisos de roles de proyecto
        """
        permisos= self.permisosRolesProyecto(uk, pk)
        return Response(permisos, status=status.HTTP_200_OK)

    def permisosRolesProyecto(self, uk, pk):
        """
        :param uk: clave primaria de usuario
        :param pk: clave primaria de roles de proyecto
        :return: una lista de los permisos de los roles de proyecto
        """
        roles_asignados = []
        response = []
        roles_user_proyecto = RolProyectoUser.objects.filter(user_id=uk).values('rol_id')
        roles_user_proyecto_ids_raw = IdsManager.ids_raw_rol(roles_user_proyecto)
        roles_proyecto = RolProyecto.objects.filter(proyecto_id=pk, fase_id=None).values('id')
        for r in roles_user_proyecto_ids_raw:
            for rp in roles_proyecto:
                if r == rp['id']:
                    roles_asignados.append(r)

        for id in roles_asignados:
            permisos_ids = RolProyectoPermiso.objects.filter(rol_id=id).values('permiso_id')
            permisos_ids_raw = IdsManager.ids_raw_permiso(permisos_ids)
            permisos = PermisoProyecto.objects.filter(id__in=permisos_ids_raw)
            for p in permisos:
                isIn = False
                for r in response:
                    if p.id == r["id"]:
                        isIn = True
                if not isIn:
                    response.append({
                        "id": p.id,
                        "permiso":p.permiso,
                        "permiso_nombre":p.permiso_nombre
                    })
        return response


class obtenerPermisosProyectoFase(APIView):

    def get(self, request, uk, pk, fk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param pk: clave primaria de permisos de roles de proyecto
        :param fk: clave primaria de fase de proyecto
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función obtiene los permisos de roles de proyecto
        """
        permisos= self.permisosRolesProyecto(uk, pk, fk)
        return Response(permisos, status=status.HTTP_200_OK)

    def permisosRolesProyecto(self, uk, pk, fk):
        """
        :param uk: clave primaria de usuario
        :param pk: clave primaria de roles de proyecto
        :return: una lista de los permisos de los roles de proyecto
        """
        roles_asignados = []
        response = []
        roles_user_proyecto = RolProyectoUser.objects.filter(user_id=uk).values('rol_id')
        roles_user_proyecto_ids_raw = IdsManager.ids_raw_rol(roles_user_proyecto)
        roles_proyecto = RolProyecto.objects.filter(proyecto_id=pk, fase_id=fk).values('id')
        for r in roles_user_proyecto_ids_raw:
            for rp in roles_proyecto:
                if r == rp['id']:
                    roles_asignados.append(r)

        for id in roles_asignados:
            permisos_ids = RolProyectoPermiso.objects.filter(rol_id=id).values('permiso_id')
            permisos_ids_raw = IdsManager.ids_raw_permiso(permisos_ids)
            permisos = PermisoProyecto.objects.filter(id__in=permisos_ids_raw)
            for p in permisos:
                isIn = False
                for r in response:
                    if p.id == r["id"]:
                        isIn = True
                if not isIn:
                    response.append({
                        "id": p.id,
                        "permiso":p.permiso,
                        "permiso_nombre":p.permiso_nombre
                    })
        return response

class eliminarUsuarioProyecto(APIView):
    def delete(self, request, uk, pk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param rk: clave primaria de rol de proyecto
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función elimina un rol de proyecto de un usuario
        """
        comite = ComiteProyecto.objects.get(proyecto=pk, miembro=uk, activo=True)
        if comite is not None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        roles_proyecto = RolProyecto.objects.filter(proyecto_id=pk).values('id')

        roles_user = RolProyectoUser.objects.filter(rol__in=roles_proyecto).filter(user_id=uk)

        for r in roles_user:
            r.delete()

        try:
            comite = get_object_or_404(ComiteProyecto, user=uk, proyecto=pk)
            comite.delete()
        except:
            print("No se encuentra en el comite")        
        return Response({"mensaje": "Usuario eliminado"}, status=status.HTTP_200_OK)

class rolesUsuario(APIView):
    def get(self, request, uk, pk):
        """
        :param request: recibe una petición HTTP
        :param uk: clave primaria de usuario
        :param rk: clave primaria de rol de proyecto
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función elimina un rol de proyecto de un usuario
        """
        roles_proyecto = RolProyecto.objects.filter(proyecto_id=pk).values('id')

        roles_user = RolProyectoUser.objects.filter(rol__in=roles_proyecto).filter(user_id=uk)

        serializer = RolProyectoUserDepthSerializer(roles_user, many=True)

        return Response(serializer.data, status=status.HTTP_200_OK)