from api.models import Fase, Proyecto, Item, RelacionItem, TipoItemFase, EstadoItem
from api.serializers.relacion_item.relacionItemSerializer import RelacionItemSerializer, RelacionItemSimpleSerializer
from api.serializers.items.itemSerializer import ItemDrawSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
from django.db import IntegrityError


class relacionItemListView(APIView):
    """
    Lista todas las fases, o crea una nueva fase.
    """

    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP
        Esta función sirve para obtener las relaciones de los proyectos y las serializa
        """
        proyecto_id = request.GET.get("proyecto")
        fases_del_proyecto = Fase.objects.filter(proyecto=proyecto_id).order_by('orden')
        tipo_item_fases = TipoItemFase.objects.filter(fase__in=fases_del_proyecto)
        item = Item.objects.filter(
            tipo__in=tipo_item_fases.values_list('id', flat=True),
            current=True
        )
        relaciones_item = RelacionItem.objects.filter(Q(item_relacion_1__in=item) | Q(item_relacion_2__in=item))

        for r in relaciones_item:
            if (not r.item_relacion_1.current) or (not r.item_relacion_2.current):
                relaciones_item = relaciones_item.exclude(id=r.id)

        relaciones_serializer = RelacionItemSerializer(relaciones_item, many=True)
        fases_list = []

        for n in fases_del_proyecto:
            tipo_item_fasen = tipo_item_fases.filter(fase=n)
            items_fase = item.filter(tipo__in=tipo_item_fasen)
            print(items_fase)
            fases_list.append(
                {
                    "fase_id": n.id,
                    "fase_nombre": n.nombre,
                    "items": ItemDrawSerializer(items_fase, many=True).data
                }
            )

        response = {
            "fases": fases_list,
            "relaciones": relaciones_serializer.data
        }
        return Response(response)

    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP de éxito o de error
        Esta función crea una relacion entre dos items, clasifica el tipo y la serializa
        """

        item1 = Item.objects.get(pk=request.data['item_relacion_1'])
        item2 = Item.objects.get(pk=request.data['item_relacion_2'])
        if item1 != item2:
            tipoitem1 = item1.tipo
            print(item1.tipo)
            tipoitem2 = item2.tipo
            fase1 = tipoitem1.fase
            fase2 = tipoitem2.fase
            relacion = RelacionItem()
            estadoAP = EstadoItem.objects.get(nombre='AP')

            if fase1.id == fase2.id:
                existe = RelacionItem.objects.filter(item_relacion_1=item2, item_relacion_2=item1, tipo='PH').exists()
                if not existe:

                    if item1.estado == estadoAP:
                        relacion.tipo = 'PH'
                        relacion.item_relacion_1 = item1
                        relacion.item_relacion_2 = item2
                    else:
                        error = {
                            "mensaje": "El item padre no está aprobado"
                        }
                        return Response(error, status=status.HTTP_400_BAD_REQUEST)
                else:
                    error = {
                        "mensaje": "La relación que se intenta crear ocasionará una relación circular"
                    }
                    return Response(error, status=status.HTTP_400_BAD_REQUEST)
            else:
                relacion.tipo = 'AS'
                if fase1.orden + 1 == fase2.orden:
                    relacion.item_relacion_1 = item1
                    relacion.item_relacion_2 = item2
                elif fase2.orden + 1 == fase1.orden:
                    relacion.item_relacion_1 = item2
                    relacion.item_relacion_2 = item1
                else:
                    error = {
                        "mensaje": "Los items no pertenecen a fases consecutivas"
                    }
                    return Response(error, status=status.HTTP_400_BAD_REQUEST)
                if relacion.item_relacion_1.estado != estadoAP:
                    error = {
                        "mensaje": "El item antecesor no está aprobado"
                    }
                    return Response(error, status=status.HTTP_400_BAD_REQUEST)

            try:
                relacion.save()
            except IntegrityError:
                error = {
                    "mensaje": "Esta relación ya existe"
                }
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
            return Response(status=status.HTTP_201_CREATED)
        else:
            error = {
                "mensaje": "No se puede relacionar un item consigo mismo"
            }
            return Response(error, status=status.HTTP_400_BAD_REQUEST)


class relacionItemDetail(APIView):

    recorridos = []

    def get(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de proyecto
        :return: una respuesta HTTP
        Esta función sirve para obtener los detalles de relacioens de un item proyecto
        """

        # Verificar que todos los items esten conectados
        estadoCA = EstadoItem.objects.get(nombre='CA')

        fases_proyecto = Fase.objects.filter(proyecto=pk)
        items_proyecto_noCA = Item.objects.filter(
            tipo__in=TipoItemFase.objects.filter(fase__in=fases_proyecto),
            current=True
        ).filter(~Q(estado=estadoCA)).count()

        if items_proyecto_noCA > 1:
            fase1 = Fase.objects.get(proyecto=pk, orden=1)
            items_fase1 = Item.objects.filter(tipo__in=TipoItemFase.objects.filter(fase=fase1.id), current=True).filter(
                ~Q(estado=estadoCA))

            for i in items_fase1:
                existe = RelacionItem.objects.filter(item_relacion_2=i).exists()
                if not existe:
                    relacionItemDetail.recorridos.clear()
                    relacionItemDetail.recorridos.append(i.id)
                    self.recorrido(i)
                    break

            if len(relacionItemDetail.recorridos) == items_proyecto_noCA:
                mensaje = {
                    "estado": 0,
                    "mensaje": "Todo esta conectado"
                }
                return Response(mensaje, status=status.HTTP_200_OK)
            else:
                mensaje = {
                    "estado": -1,
                    "mensaje": "El proyecto tiene items aislados que deben ser relacionados"
                }
                return Response(mensaje, status=status.HTTP_200_OK)
        else:
            mensaje = {
                "estado": 1,
                "mensaje": "El proyecto aun no posee suficientes items"
            }
            return Response(mensaje, status=status.HTTP_200_OK)

    def recorrido(self, item):
        """
        :param request: item
        :return: none
        Esta función realiza el recorrido
        """
        if item.current:
            relacionesA = RelacionItem.objects.filter(item_relacion_1=item)
            for i in relacionesA:
                if (not (i.item_relacion_2.id in relacionItemDetail.recorridos)) and i.item_relacion_2.current:
                    relacionItemDetail.recorridos.append(i.item_relacion_2.id)
                    print(i.item_relacion_2)
                    self.recorrido(i.item_relacion_2)

    def get_object(self, pk):
        """

        :param pk: clave primaria de línea base
        :return: un objeto línea base
        Esta función retorna un objeto de línea base
        """
        try:
            return RelacionItem.objects.get(pk=pk)
        except RelacionItem.DoesNotExist:
            raise Http404

    def delete(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de fase
        :return: una respuesta HTTP
        Esta función elimina una fase de un proyecto
        """
        relacion = self.get_object(pk)
        relacion.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)