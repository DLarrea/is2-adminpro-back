from rest_framework import generics
from api.models import EstadoItem
from api.serializers.items.estadoItemSerializer import EstadoItemSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

class ItemEstadoList(generics.ListCreateAPIView):
    queryset = EstadoItem.objects.all()
    serializer_class = EstadoItemSerializer

class ItemEstadoDetail(APIView):
    def get(self, request, pk):

        """

        :param request: recibe un mensaje HTTP
        :param pk: clave primaria de estado de ítem
        :return: una respuesta HTTP serializada
        Esta función obtiene el estado de un ítem
        """

        estado = EstadoItem.objects.get(pk=pk)
        
        
        # Estado EN DESARROLLO
        if estado.nombre == "EN DESARROLLO":
            estados_others = EstadoItem.objects.all().exclude(nombre__in=["APROBADO", "RECHAZADO"])

        if estado.nombre == "PENDIENTE APROBACIÓN":
            estados_others = EstadoItem.objects.all().exclude(nombre__in=["EN DESARROLLO", "DESHABILITADO"])

        if estado.nombre == "APROBADO":
            estados_others = EstadoItem.objects.all().exclude(nombre__in=["PENDIENTE APROBACIÓN", "RECHAZADO", "DESHABILITADO"])

        if estado.nombre == "RECHAZADO":
            estados_others = EstadoItem.objects.all().exclude(nombre__in=["PENDIENTE APROBACIÓN", "APROBADO", "DESHABILITADO"])

        if estado.nombre == "DESHABILITADO":
            estados_others = EstadoItem.objects.all().exclude(nombre__in=["PENDIENTE APROBACIÓN", "APROBADO", "RECHAZADO"])

        serializer = EstadoItemSerializer(estados_others, many=True)

        return Response(serializer.data)