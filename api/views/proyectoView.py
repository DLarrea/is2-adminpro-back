from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.shortcuts import get_object_or_404, get_list_or_404
from django.db import connection
from django.contrib.auth.models import User
from api.models import Proyecto, EstadoProyecto, RolProyecto, PermisoProyecto, RolProyectoUser, RolProyectoPermiso, Fase
from api.serializers.fases.faseSerializer import FaseSerializer
from api.serializers.proyectos.proyectoSerializer import ProyectoSerializer, ProyectoDepthSerializer, ProSerializer
from api.serializers.proyectos.estadoProyectoSerializer import EstadoProyectoSerializer
from api.serializers.roles.rolProyectoSerializer import RolProyectoSerializer
from api.serializers.usuarios.userSerializer import UserSerializer

import json

class getPostView(APIView):

    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP serializada
        Esta función obtiene los estados de un proyecto
        """
        proyectos = Proyecto.objects.all()
        estados = EstadoProyecto.objects.all()
        result = []
        for p in proyectos:
            estado = {}
            for e in estados:
                if e.id == p.estadoProyecto_id:
                    estado = e
            item = {
                "nombre": ProyectoSerializer(p).data.get("nombre"),
                "descripcion": ProyectoSerializer(p).data.get("descripcion"),
                "estado": EstadoProyectoSerializer(estado).data.get("nombre")
            }
            result.append(item)
        # serializer = ProyectoSerializer(proyectos, many=True)
        return Response(result)

    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito o de error
        Esta función pone estado de proyecto en PENDIENTE, crea el rol de gerente de Proyecto, asigna los permisos de proyectos al gerente y asigna el rol de proyecto al gerente
        """

        proyecto = request.data

        #Setear estado PENDIENTE
        estado_pendiente = EstadoProyecto.objects.get(nombre="PENDIENTE")
        id_e_p = estado_pendiente.id
        proyecto["estadoProyecto"] = id_e_p
        print(proyecto)
        s_proyecto = ProSerializer(data=proyecto)
        if s_proyecto.is_valid():
            s_proyecto.save()

            # Se crea rol gerente para el proyecto
            rol_gerente = RolProyecto.objects.create(nombre="Gerente", proyecto_id=s_proyecto.data.get("id"))
            rol_gerente.save()

            # Se asignan todos los permisos sobre proyectos al rol
            permisos = PermisoProyecto.objects.filter(tipo_permiso="P")

            for p in permisos:
                per = RolProyectoPermiso.objects.create(permiso_id=p.id, rol_id=rol_gerente.id)
                per.save()

            # Se asigna el rol de proyecto al usuario
            proyecto_rol_user = RolProyectoUser.objects.create(rol_id=rol_gerente.id, user_id=proyecto["gerente_id"])
            proyecto_rol_user.save()

            return Response(s_proyecto.data, status=status.HTTP_201_CREATED)
        return Response(s_proyecto.errors, status=status.HTTP_400_BAD_REQUEST)

class getPutDeleteView(APIView):

    def get_object(self, pk):
        """

        :param pk: clave primaria de proyecto
        :return: el proyecto de acuerdo a su clave primaria
        Esta función obtiene un proyecto
        """
        try:
            return Proyecto.objects.get(pk=pk)
        except Proyecto.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de proyecto
        :param format:
        :return: una respuesta serializada
        Esta función serializa un proyecto
        """
        proyecto = self.get_object(pk)
        serializer = ProyectoDepthSerializer(proyecto)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de proyecto
        :param format:
        :return:una respuesta HTTP serializada
        Esta función serializa un proyecto y guarda en la BD
        """
        proyecto = self.get_object(pk)
        serializer = ProSerializer(proyecto, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class getProyectosUserView(APIView):
    def get(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de rol de proyecto
        :param format:
        :return: devuelve una respuesta HTTP serializada
        Esta función obtiene los roles de un proyecto
        """
        roles_user = RolProyectoUser.objects.filter(user_id=pk).values('rol_id')
        proyectos_ids = RolProyecto.objects.filter(id__in=roles_user).values('proyecto_id')
        proyectos = Proyecto.objects.filter(id__in=proyectos_ids)
        s_proyectos = ProyectoSerializer(proyectos, many=True)

        return Response(s_proyectos.data)

class getMiembrosProyectoView(APIView):
    def get(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de rol de proyecto
        :param format:
        :return: una respuesta HTTP
        Esta función lista los roles de un proyecto de un usuario
        """
        roles_proyectos = RolProyecto.objects.filter(proyecto_id=pk).values('id')
        print(roles_proyectos)
        roles_user = RolProyectoUser.objects.all()
        user_ids = []
        for ru in roles_user:
            isIn = False
            for rp in roles_proyectos:
                if ru.rol_id == rp["id"]:
                    isIn = True
                    break
            if isIn:
                user_ids.append(ru.user_id)
        users = User.objects.filter(id__in=user_ids)
        s_users = UserSerializer(users, many=True)

        return Response(s_users.data)

class postMiembroProyectoView(APIView):
    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxtito
        Esta función crea un rol de proyecto para un usuario
        """
        roles = request.data.get("roles")
        user = request.data.get("usuario")
        for r in roles:
            user_rol_proyecto = RolProyectoUser.objects.create(user_id=user["id"], rol_id=r["id"])
            user_rol_proyecto.save()
        return Response({"mensaje":"Usuario agregado"})

class getFasesProyecto(APIView):
    def get(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de proyecto
        :return: una respuesta HTTP serializada
        Esta función obtiene y lista las fases de un proyecto
        """
        fasesproyecto = get_list_or_404(Fase, proyecto_id=pk)
        print(fasesproyecto)
        serializer = FaseSerializer(fasesproyecto, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

class iniciarProyectoView(APIView):
    
    def get_object(self, pk):
        """

        :param pk: clave primaria de proyecto
        :return: el proyecto de acuerdo a su clave primaria
        Esta función obtiene un proyecto
        """
        try:
            return Proyecto.objects.get(pk=pk)
        except Proyecto.DoesNotExist:
            raise Http404
    
    def put(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de proyecto
        :param format:
        :return:una respuesta HTTP serializada
        Esta función serializa un proyecto y guarda en la BD
        """
        proyecto = self.get_object(pk)
        serializer = ProSerializer(proyecto, data=request.data)
        if serializer.is_valid():
            has_fase = Fase.objects.filter(proyecto=pk).count()
            if has_fase > 0:
                serializer.save()
                fase_inicial = Fase.objects.get(proyecto=pk, orden=1)
                fase_inicial.estado_fase = 'AB'
                fase_inicial.save()
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
            return Response({"mensaje":"Proyecto iniciado"})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
