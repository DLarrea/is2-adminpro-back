from rest_framework import generics
from api.models import TipoItem, TipoItemAtributo, Atributo
from api.serializers.items.tipoItemSerializer import TipoItemSerializer
from api.serializers.atributos.atributosSerializer import AtributoSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from api.utils.ids import IdsManager
import json

class tipoItemList(APIView):

    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito (creado) o de error
        Esta función obtiene y serializa un tipo de ítem
        """
        fase_id = request.GET.get("fase")
        tipoItems = TipoItem.objects.filter(fase_id=fase_id)
        serializer = TipoItemSerializer(tipoItems, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito (creado) o de error
        Esta función crea un tipo de ítem serializado
        """
        serializer = TipoItemSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class tipoItemDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = TipoItem.objects.all()
    serializer_class = TipoItemSerializer

class tipoItemGestionar(APIView):
    def get(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de tipo de ítem
        :return: una respuesta HTTP de éxito (status 200)
        Esta función obtiene los atributos de un tipo de ítem
        """
        atributos_asignados = TipoItemAtributo.objects.filter(tipoitem_id=pk).values_list("atributo_id", flat=True)
        aa = Atributo.objects.filter(id__in=atributos_asignados)
        ad = Atributo.objects.exclude(id__in=atributos_asignados)
        response = {
            "asignados": AtributoSerializer(aa, many=True).data,
            "disponibles": AtributoSerializer(ad, many=True).data
        }
        return Response(response, status=status.HTTP_200_OK)

    def post(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria tipo de ítem
        :return: una respuesta HTTP de éxito (status 200 OK)
        Esta función crea un atributo de un tipo de ítem
        """
        atributo = Atributo.objects.get(pk=request.GET.get("atributo"))
        tipoItem = TipoItem.objects.get(pk=pk)
        tipoItem_atributo = TipoItemAtributo.objects.create(tipoitem=tipoItem,atributo=atributo)
        tipoItem_atributo.save()
        return Response(status=status.HTTP_200_OK)

    def delete(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de tipo de ítem
        :return: una respuesta HTTP de éxito
        Esta función elimina un atributo de tipo de ítem
        """
        atributo = Atributo.objects.get(pk=request.GET.get("atributo"))
        tipoItem = TipoItem.objects.get(pk=pk)
        tipoItem_atributo = TipoItemAtributo.objects.get(tipoitem=tipoItem,atributo=atributo)
        tipoItem_atributo.delete()
        return Response(status=status.HTTP_200_OK)

class tipoItemImportar(APIView):
    def get(self, request):
        """

        :param request: recibe una petición HTTP
        :return: un serializer
        Esta función obtiene y serializa tipos de ítem y atributos
        """
        response = []
        tipoItems = TipoItem.objects.exclude(proyecto_id=request.GET.get("proyecto"))

        for ti in tipoItems:
            atributos = Atributo.objects.filter(
                id__in = TipoItemAtributo.objects.filter(tipoitem_id=ti.id).values_list("atributo_id", flat=True)
            )
            response.append(
                {
                    "tipoItem": TipoItemSerializer(ti).data,
                    "atributos": AtributoSerializer(atributos, many=True).data
                }
            )
        return Response(response)
    def post(self, request):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP de éxito (status 200 OK) o de error
        Esta función guarda un nuevo tipo de ítem
        """
        json_data = json.loads(request.body)
        tipoItem = json_data["tipoItem"]
        atributos = json_data["atributos"]
        serializer = TipoItemSerializer(data=tipoItem)
        if serializer.is_valid():
            serializer.save()
            new_tipoitem = TipoItem.objects.get(pk=serializer.data.get("id"))

            for a in atributos:
                atributo = Atributo.objects.get(pk=a["id"])
                tipoItemAtributo = TipoItemAtributo.objects.create(tipoitem=new_tipoitem,atributo=atributo)
                tipoItemAtributo.save()
            return Response(status=status.HTTP_200_OK)
        return Response(status=status.HTTP_400_BAD_REQUEST)

class tipoItemAtributos(APIView):
    def get(self, request, pk):
        """

        :param request: recibe un petición HTTP
        :param pk: clave primaria de tipo de ítem
        :return: un serializer
        Esta función obtiene la lista de atributos de un tipo de ítem
        """
        atributos = Atributo.objects.filter(
            id__in= TipoItemAtributo.objects.filter(tipoitem=pk).values_list('atributo', flat=True)
        )
        serializer = AtributoSerializer(atributos, many=True)
        return Response(serializer.data)


class tipoItemListProyecto(APIView):

    def get(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito (creado) o de error
        Esta función obtiene y serializa un tipo de ítem
        """
        tipoItems = TipoItem.objects.filter(proyecto_id=pk)
        serializer = TipoItemSerializer(tipoItems, many=True)
        return Response(serializer.data)
