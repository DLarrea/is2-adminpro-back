from api.models import Fase, Proyecto, EstadoProyecto, TipoItemFase, Item, EstadoItem
from api.serializers.fases.faseSerializer import FaseSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.shortcuts import get_object_or_404, get_list_or_404

import json

class faseListView(APIView):
    """
    Lista todas las fases, o crea una nueva fase.
    """
    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP
        Esta función sirve para obtener las fases de los proyectos y las serializa
        """
        proyecto_id = request.GET.get("proyecto")
        fases = Fase.objects.filter(proyecto_id=proyecto_id).order_by('orden')
        serializer = FaseSerializer(fases, many=True)
        return Response(serializer.data)


    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito o de error
        Esta función crea una fase y la serializa
        """
        fases_orden = Fase.objects.filter(proyecto=request.data['proyecto']).count()
        fase = {
            'nombre': request.data['nombre'],
            'descripcion': request.data['descripcion'],
            'proyecto': request.data['proyecto'],
            'orden': fases_orden + 1
        }
        serializer = FaseSerializer(data=fase)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class faseDetailView(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    def get_object(self, pk):
        """

        :param pk: clave primaria de una fase
        :return: la fase con la clave primaria
        """
        try:
            return Fase.objects.get(pk=pk)
        except Fase.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de una fase
        :param format:
        :return: una respuesta HTTP serialzada
        """
        fase = self.get_object(pk)
        serializer = FaseSerializer(fase)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de fase
        :param format:
        :return: una respuesta http de éxito o de error
        Esta función guarda una fase creada
        """
        fase = self.get_object(pk)

        if request.data['estado_fase'] == 'CE':
            estadoCA = EstadoItem.objects.get(nombre='CA')
            items = Item.objects.filter(
                tipo__in=TipoItemFase.objects.filter(fase=fase).values_list('id', flat=True),
                current=True
            ).exclude(estado=estadoCA)
            estadoAP = EstadoItem.objects.get(nombre='AP')
            for i in items:
                if (i.estado.id != estadoAP.id) or (i.lineaBase is None):
                    error = {
                        "mensaje": "Todos los items deben estar aprobados y en una Linea Base"
                    }
                    return Response(error, status=status.HTTP_400_BAD_REQUEST)

        serializer = FaseSerializer(fase, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de fase
        :param format:
        :return: una respuesta HTTP
        Esta función elimina una fase de un proyecto
        """
        fase = self.get_object(pk)
        fase.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class IniciarProyectoFase(APIView):
    def put(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de una fase
        :return: una respuesta HTTP de éxito
        Esta función obtiene una fase, cambia su estado, lo guarda e incia y almacena un proyecto, también lo pone en dearrollo
        """
        fase = Fase.objects.get(pk=pk)
        fase.estado_fase = "AB"
        fase.save()

        #Iniciamos proyecto
        estadoProyecto = EstadoProyecto.objects.get(nombre="EN DESARROLLO")

        proyecto = Proyecto.objects.get(pk=fase.proyecto_id)
        proyecto.estadoProyecto = estadoProyecto
        proyecto.save()

        return Response(status=status.HTTP_200_OK)
