from rest_framework import generics
from api.models import EstadoProyecto
from api.serializers.proyectos.estadoProyectoSerializer import EstadoProyectoSerializer


class EstadoProyectoList(generics.ListCreateAPIView):
    queryset = EstadoProyecto.objects.all()
    serializer_class = EstadoProyectoSerializer


class EstadoProyectoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = EstadoProyecto.objects.all()
    serializer_class = EstadoProyectoSerializer