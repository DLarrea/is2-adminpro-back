from rest_framework import generics
from api.models import Item, TipoItem, TipoItemFase, Fase, RelacionItem, EstadoItem, RelacionItem
from api.serializers.items.itemSerializer import ItemSerializer, ItemSaveSerializer, ItemsProyectoSerializer, \
    ItemDrawSerializer, ItemsRevisionSerializer, ItemSerializer2
from api.serializers.items.estadoItemSerializer import EstadoItemSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q
import json


class ItemList(APIView):
    def get(self, request, format=None):
        """

        :param request: recibe una respuesta HTTP
        :param format:
        :return: una respuesta HTTP serializada con los ítems de un tipo de una fase
        Esta función obtiene los ítems de una fase
        """
        fase_id = request.GET.get("fase")

        lb_id = request.GET.get("lb", None)

        if lb_id is None:
            items = Item.objects.filter(
                tipo__in=TipoItemFase.objects.filter(fase=fase_id).values_list('id', flat=True),
                current=True
            )
        else:
            items = Item.objects.filter(
                tipo__in=TipoItemFase.objects.filter(fase=fase_id).values_list('id', flat=True),
                current=True,
                lineaBase=lb_id
            )

        serializer = ItemSerializer(items, many=True)

        return Response(serializer.data)

    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de error si el serializer es inválido
        Esta función crea un ítem y verifica sus relaciones y si lo actualiza
        """
        # model
        # {
        #     prev_id: x
        #     item: x
        # }

        body_unicode = request.body.decode('utf-8')
        body_data = json.loads(body_unicode)

        # Guardarmos el item (Ya debe traer current=True)
        serializer = ItemSaveSerializer(data=body_data.get("item", None))
        if serializer.is_valid():
            serializer.save()
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        # Verificamos si existe un item previo
        previous_item_id = body_data.get("prev_id", None)

        if previous_item_id is not None:
            # Seteamos el siguiente al item anterior y le quitamos current
            previous_item = Item.objects.get(pk=previous_item_id)
            current_item = Item.objects.get(pk=serializer.data.get("id"))

            previous_item.sig = current_item
            previous_item.current = False
            previous_item.save()

            # Seteamos el anterior al item actual

            current_item.ant = previous_item
            current_item.save()

            relaciones_item = RelacionItem.objects.filter(
                Q(item_relacion_1=previous_item) | Q(item_relacion_2=previous_item))

            for r in relaciones_item:
                if r.item_relacion_1 == previous_item:
                    r.item_relacion_1 = current_item
                    r.save()
                elif r.item_relacion_2 == previous_item:
                    r.item_relacion_2 = current_item
                    r.save()

        return Response(serializer.data)


class ItemsProyectoList(APIView):
    def get(self, request, pk, format=None):
        """
        :param pk: Id del proyecto
        :return: una respuesta HTTP serializada con los ítems de un proyecto
        Esta función obtiene los ítems de un proyecto
        """
        fases_del_proyecto = Fase.objects.filter(proyecto=pk).order_by('orden')
        tipo_item_fases = TipoItemFase.objects.filter(fase__in=fases_del_proyecto)
        items = Item.objects.filter(
            tipo__in=tipo_item_fases.values_list('id', flat=True),
            current=True
        )
        serializer = ItemsProyectoSerializer(items, many=True)

        return Response(serializer.data)


class RevisionItemsList(APIView):
    def get(self, request, pk, format=None):
        """
        :param pk: Id del proyecto
        :return: una respuesta HTTP serializada con los ítems de un proyecto
        Esta función obtiene los ítems con estado "En Revisión" de un proyecto
        """
        fases_del_proyecto = Fase.objects.filter(proyecto=pk).order_by('orden')
        tipo_item_fases = TipoItemFase.objects.filter(fase__in=fases_del_proyecto)
        estadoRE = EstadoItem.objects.get(nombre='RE')
        estadoDE = EstadoItem.objects.get(nombre='DE')
        items = Item.objects.filter(
            tipo__in=tipo_item_fases.values_list('id', flat=True),
            current=True,
            estado=estadoRE
        )
        serializer = ItemSerializer2(items, many=True)

        response = {
            "items": serializer.data,
            "estadoDE": EstadoItemSerializer(estadoDE).data
        }
        return Response(response)


class ImpactoItemsList(APIView):
    item_recorridos = []
    calc_impacto = 0

    def get(self, request, pk, format=None):
        """
        :param request: recibe una petición HTTP
        :param pk: clave primaria de ítem
        :return: una respuesta HTTP con estado 200 indicando el impacto de  modificación del ítem
        Esta función obtiene el impacto de cada ítem
        """
        item = Item.objects.get(id=pk)
        ImpactoItemsList.item_recorridos.clear()
        ImpactoItemsList.calc_impacto = 0
        self.impacto(item)
        items_afectado = Item.objects.filter(id__in=ImpactoItemsList.item_recorridos)
        response = {
            "impacto": ImpactoItemsList.calc_impacto,
            "items_afectados": ItemDrawSerializer(items_afectado, many=True).data
        }
        return Response(response,status=status.HTTP_200_OK)

    def impacto(self, item):
        """
        :param item: ítem 
        :return: nada
        Esta función es un método interno
        """
        if item.current:
            relacionesA = RelacionItem.objects.filter(item_relacion_1=item)
            for i in relacionesA:
                if (not (i.item_relacion_2.id in ImpactoItemsList.item_recorridos)) and i.item_relacion_2.current:
                    ImpactoItemsList.item_recorridos.append(i.item_relacion_2.id)
                    ImpactoItemsList.calc_impacto = ImpactoItemsList.calc_impacto + i.item_relacion_2.prioridad
                    self.impacto(i.item_relacion_2)


class ItemDetail(APIView):
    def get_object(self, pk):
        """

        :param pk: clave primaria de tipo de ítem
        :return: tiop de ítem

        Esta función retorna los objetos tipo de ítem
        """
        try:
            return Item.objects.get(pk=pk)
        except Item.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de ítem
        :param format:
        :return: una respuesta HTTP serializada del objeto ítem

        Esta función obtiene un ítem de acuerdo a su id
        """
        item = self.get_object(pk)
        serializer = ItemSerializer(item)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de ítem
        :param format:
        :return: una respuesta http de error si el serializer es inválido
        Esta función obtiene un ítem de acuerdo a su id y hace un response
        """
        item = self.get_object(pk)
        estadoCA = EstadoItem.objects.get(nombre='CA')
        estadoRE = EstadoItem.objects.get(nombre='RE')
        estadoAP = EstadoItem.objects.get(nombre='AP')
        if request.data['estado'] == estadoCA.id:
            relaciones = RelacionItem.objects.filter(Q(item_relacion_1=item) | Q(item_relacion_2=item)).exists()
            if relaciones:
                error = {
                    "mensaje": "El item tiene relaciones que deben ser gestionadas antes de ser cancelado"
                }
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
        if request.data['estado'] == estadoAP.id:
            print("a aprobar", item)
            tipoit = TipoItemFase.objects.get(id=item.tipo.id)
            print("tipo", tipoit)
            fase = Fase.objects.get(id=tipoit.fase.id)
            print(fase)
            fases_del_proyecto = Fase.objects.filter(proyecto=fase.proyecto.id)
            print(fases_del_proyecto)
            tipo_item_fases = TipoItemFase.objects.filter(fase__in=fases_del_proyecto)
            items = Item.objects.filter(
                tipo__in=tipo_item_fases.values_list('id', flat=True),
                current=True
            ).count()
            print("cant", items)
            relaciones = RelacionItem.objects.filter(item_relacion_2=item).exists()
            if not (relaciones or ((items == 1) and (fase.orden == 1))):
                error = {
                    "mensaje": "El item necesita estar relacionado con un padre o un antecesor para ser aprobado"
                }
                return Response(error, status=status.HTTP_400_BAD_REQUEST)
        serializer = ItemSaveSerializer(item, data=request.data)
        print(serializer)
        if request.data['estado'] == estadoRE.id:
            itemn = {
                'nombre': request.data['nombre'],
                'descripcion': request.data['descripcion'],
                'version': request.data['version'],
                'prioridad': request.data['prioridad'],
                'lineaBase': request.data['lineaBase'],
                'estado': item.estado_ant,
                'estado_ant': None,
                'tipo': request.data['tipo'],
                'ant': request.data['ant'],
                'sig': request.data['sig'],
                'current': request.data['current'],
                'values': request.data['values']
            }
            serializer = ItemSaveSerializer(data=itemn)
        print("serializerRE", serializer)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ItemHistorial(APIView):
    def get(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de ítem
        :return: un serializer de ítem
        Esta función obtiene un ítem de la base de datos
        """
        has_items = True
        result = []
        current_item = Item.objects.get(pk=pk)
        result.append(current_item)

        while has_items:
            if current_item.ant is not None:
                current_item = Item.objects.get(pk=current_item.ant.id)
                result.append(current_item)
            else:
                has_items = False
        serializer = ItemSerializer(result, many=True)
        return Response(serializer.data)

    def post(self, request, pk):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de ítem
        :return: una respuesta HTTP de éxito o de error serializada
        Esta función crea un ítem y establece sus relaciones
        """

        # {
        #     current_id: x,
        #     item:  x
        # } 

        item_to_restore = Item.objects.get(pk=pk)

        if item_to_restore.ant is None and item_to_restore.sig is not None:  # Caso 1 (Sig True, Ant False)
            item_sig = Item.objects.get(pk=item_to_restore.sig.id)
            item_sig.ant = None
            item_sig.save()
            item_to_restore.delete()
        elif item_to_restore.ant is not None and item_to_restore.sig is not None:  # Caso 2 (Sig True, Ant True)

            item_sig = Item.objects.get(pk=item_to_restore.sig.id)
            item_ant = Item.objects.get(pk=item_to_restore.ant.id)

            item_sig.ant = item_ant
            item_sig.save()

            item_ant.sig = item_sig
            item_ant.save()

            item_to_restore.delete()

        body_unicode = request.body.decode('utf-8')
        body_data = json.loads(body_unicode)

        serializer = ItemSaveSerializer(data=body_data.get("item", None))

        if serializer.is_valid():
            serializer.save()
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        current_item = Item.objects.get(pk=body_data.get("current_id"))
        new_item = Item.objects.get(pk=serializer.data.get("id"))

        current_item.sig = new_item
        current_item.current = False
        current_item.save()

        new_item.current = True
        new_item.version = current_item.version + 1
        new_item.ant = current_item
        new_item.sig = None
        new_item.save()

        serializer = ItemSaveSerializer(new_item)
        return Response(serializer.data)


class ItemLineaBaseGet(APIView):
    def get(self, request, format=None):
        """

        :param request: recibe una respuesta HTTP
        :param format:
        :return: una respuesta HTTP serializada
        Esta función obtiene un ítem de una fase
        """
        lb_id = request.GET.get("lb")

        items = Item.objects.filter(
            lineaBase=lb_id
        )

        serializer = ItemSerializer(items, many=True)

        return Response(serializer.data)
