from api.models import RolProyecto, PermisoProyecto, RolProyectoPermiso, RolProyectoUser
from api.serializers.roles.rolProyectoSerializer import RolProyectoSerializer
from api.serializers.permisos.permisoProyectoSerializer import PermisoProyectoSerializer
from api.serializers.roles_permisos.rolProyectoPermisoSerializer import RolProyectoPermisoSerializer
from api.serializers.roles_usuarios.rolProyectoUserSerializer import RPUSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.shortcuts import get_object_or_404, get_list_or_404
from django.db import connection

import json

class postView(APIView):

    def post(self, request, format=None):
        """
        :param request: Petición HTTP POST
        :return: RolProyecto
        """
        serializer = RolProyectoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class getRolesProyectoView(APIView):

    def get(self, request, pk, format=None):
        """
        :param request: Petición HTTP GET
        :param pk: id Proyecto
        :return: RolesProyecto
        """
        roles = RolProyecto.objects.filter(proyecto_id=pk, fase_id=None)
        serializer = RolProyectoSerializer(roles, many=True)
        return Response(serializer.data)

class getRolesProyectoFaseView(APIView):

    def get(self, request, pk, fk, format=None):
        """
        :param request: Petición HTTP GET
        :param pk: id Proyecto
        :return: RolesProyecto
        """
        roles = RolProyecto.objects.filter(proyecto_id=pk, fase_id=fk)
        serializer = RolProyectoSerializer(roles, many=True)
        return Response(serializer.data)

class getPutDeleteView(APIView):

    def get_object(self, pk):
        """
        :param pk: id RolProyecto
        :return: RolProyecto
        """
        try:
            return RolProyecto.objects.get(pk=pk)
        except RolProyecto.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """
        :param request: Petición HTTP GET
        :param pk: id RolProyecto
        :param format:
        :return: RolProyecto
        """
        rol = self.get_object(pk)
        serializer = RolProyectoSerializer(rol)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """
        :param request: Petición HTTP PUT
        :param pk: id RolProyecto
        :param format:
        :return: RolProyecto
        """
        rol = self.get_object(pk)
        serializer = RolProyectoSerializer(rol, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """
        :param request: Petición HTTP DELETE
        :param pk: idRolProyecto
        :param format:
        :return: HTTP 204
        """
        rol = self.get_object(pk)
        rol_permisos = RolProyectoPermiso.objects.filter(rol_id=pk)
        print(rol_permisos)
        if rol_permisos.count() != 0:
            return Response({"error": True, "mensaje": "Existen permisos asociados"}, status=status.HTTP_400_BAD_REQUEST)
        rol_user = RolProyectoUser.objects.filter(rol_id=pk)
        if rol_user.count() != 0:
            return Response({"error": True, "mensaje": "Existen usuarios asociados"}, status=status.HTTP_400_BAD_REQUEST)
        rol.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class asignarEliminarPermisoView(APIView):
    def post(self, request, rk, pk):
        """
        :param request:
        :param rk: id RolProyecto
        :param pk: id PermisoProyecto
        :return: RolProyectoPermiso
        """
        rol = RolProyecto.objects.get(pk=rk)
        permiso = PermisoProyecto.objects.get(pk=pk)
        rol_permiso = RolProyectoPermiso.objects.create(rol_id= rol.id, permiso_id=permiso.id)
        rol_permiso.save()
        s_rol_permiso = RolProyectoPermisoSerializer(rol_permiso)
        return Response(s_rol_permiso.data)

    def delete(self, request, rk, pk):
        """
        :param request:
        :param rk: id RolProyecto
        :param pk: id PermisoProyecto
        :return: HTTP 204
        """
        rol_permiso = RolProyectoPermiso.objects.get(rol_id=rk, permiso_id=pk)
        rol_permiso.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class obtenerPermisosView(APIView):
    def get(self, request, pk):
        """

        :param request:
        :param pk: id Proyecto
        :return: Permisos disponibles y asignados
        """
        rol = RolProyecto.objects.get(pk=pk)
        s_rol = RolProyectoSerializer(rol)
        roles_permisos = RolProyectoPermiso.objects.filter(rol_id=pk)
        ids = []
        for rp in roles_permisos:
            ids.append(rp.permiso_id)
        permisos_asignados = PermisoProyecto.objects.filter(id__in=ids)
        s_permisos_asignados = PermisoProyectoSerializer(permisos_asignados, many=True)
        permisos_disponibles = PermisoProyecto.objects.all().exclude(id__in=ids)
        s_permisos_disponibles = PermisoProyectoSerializer(permisos_disponibles, many=True)

        response = {
            "rol": s_rol.data,
            "permisos_disponibles": s_permisos_disponibles.data,
            "permisos_asignados": s_permisos_asignados.data
        }
        return Response(response)



class postRolFase(APIView):

    def post(self, request, format=None):
        """
        :param request: Petición HTTP POST
        :return: RolProyecto
        """
        serializer = RPUSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class putRolFase(APIView):

    def get_object(self, pk, uk):
        """
        :param pk: id RolProyecto
        :return: RolProyecto
        """
        try:
            return RolProyectoUser.objects.get(rol=pk, user=uk)
        except RolProyectoUser.DoesNotExist:
            raise Http404

    def put(self, request, pk, uk, format=None):
        """
        :param request: Petición HTTP PUT
        :param pk: id RolProyecto
        :param format:
        :return: RolProyecto
        """
        rol = self.get_object(pk, uk)
        serializer = RPUSerializer(rol, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class obtenerPermisosFaseView(APIView):
    def get(self, request, pk, uk):
        """

        :param request:
        :param pk: fase id
        :param uk: user id
        :return: Permisos asignados
        """
        roles_fase = RolProyecto.objects.filter(fase=pk)
        
        s_rol = RolProyectoSerializer(rol)
        roles_permisos = RolProyectoPermiso.objects.filter(rol_id=pk)
        ids = []
        for rp in roles_permisos:
            ids.append(rp.permiso_id)
        permisos_asignados = PermisoProyecto.objects.filter(id__in=ids)
        s_permisos_asignados = PermisoProyectoSerializer(permisos_asignados, many=True)
        permisos_disponibles = PermisoProyecto.objects.all().exclude(id__in=ids)
        s_permisos_disponibles = PermisoProyectoSerializer(permisos_disponibles, many=True)

        response = {
            "rol": s_rol.data,
            "permisos_disponibles": s_permisos_disponibles.data,
            "permisos_asignados": s_permisos_asignados.data
        }
        return Response(response)