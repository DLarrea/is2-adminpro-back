from api.models import TipoItemFase
from api.serializers.items.tipoItemFaseSerializer import TipoItemFaseSerializer, TipoItemFaseSerializerList
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

import json

class tipoItemFaseList(APIView):
    """
    Lista todos los tipo de items de una fase o los crea.
    """
    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP
        Esta función sirve para obtener la lista de tipo de ítem
        """
        fase_id = request.GET.get("fase")
        tipos = TipoItemFase.objects.filter(fase=fase_id)
        serializer = TipoItemFaseSerializerList(tipos, many=True)
        return Response(serializer.data)


    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP de éxito o de error
        Esta función crea un tipo de ítem y hace un post de tipo de ítem
        """
        serializer = TipoItemFaseSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class tipoItemFaseDetail(APIView):
    """
    Clase de tipos de ítem.
    """
    def get_object(self, pk):
        """

        :param pk: clave primaria de un tipo de ítem
        :return: la fase con la clave primara del objeto de tipo de ítem o un error
        Esta función retorna un tipo de ítem
        """
        try:
            return TipoItemFase.objects.get(pk=pk)
        except TipoItemFase.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de un tipo de ítem
        :return: una respuesta HTTP serializada
        Esta función obtiene un objeto de tipo de ítem y lo devuelve como petición HTTP
        """
        tipo = self.get_object(pk)
        serializer = TipoItemFaseSerializer(tipo)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de tipo de ítem
        :return: una respuesta http de éxito o de error
        Esta función guarda un tipo de ítem creado
        """
        tipo = self.get_object(pk)
        serializer = TipoItemFaseSerializer(tipo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de tipo de ítem
        :return: Respuesta HTTP
        Esta función elimina una fase de un proyecto
        """
        fase = self.get_object(pk)
        fase.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

class atributosTipoItemFase(APIView):
    """
    Lista todas los atributos de ítem.
    """
    def get(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :return: una respuesta HTTP
        Esta función sirve para obtener las tipos de ítem de los proyectos y las serializa
        """
        fase_id = request.GET.get("fase")
        tipos = TipoItemFase.objects.filter(fase=fase_id)
        serializer = TipoItemFaseSerializerList(tipos, many=True)
        return Response(serializer.data)