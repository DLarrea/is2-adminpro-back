from rest_framework import generics
from rest_framework.views import APIView
from api.models import LineaBase, EstadoLineaBase, Item, Fase
from api.serializers.linea_base.lineaBaseSerializer import LineaBaseSerializer, LineaBaseDepthSerializer
from api.serializers.linea_base.estadoLineaBaseSerializer import EstadoLineaBaseSerializer
from rest_framework.response import Response
from rest_framework import status
from django.http import Http404


class LineaBaseList(APIView):
    """
    Clase de Línea Base y sus funciones de crear, obtener y establecer estado
    """

    def get(self, request, format=None):
        """

        :param request: rebibe una petición HTTP
        :param format:
        :return: una respuesta HTTP serializada

        Esta función obtiene una linea base de una fase
        """

        fase_id = request.GET.get("fase")
        lineas_base = LineaBase.objects.filter(fase_id=fase_id)
        serializer = LineaBaseDepthSerializer(lineas_base, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        """

        :param request: recibe una petición HTTP
        :param format:
        :return: una respuesta HTTP de éxito o de errorserializada de un objeto línea base

        Esta función crea una línea base o devuelve un error
        """
        print(request.data)
        cant_lineasbase = LineaBase.objects.filter(fase_id=request.data['fase']).count()
        fase = Fase.objects.get(pk=request.data['fase'])
        lineabase = {
            'nombre': request.data['nombre'],
            'descripcion': request.data['descripcion'],
            'codigo': "LB_{0}-{1}".format(fase.orden,cant_lineasbase+1),
            'observacion': request.data['observacion'],
            'estado': request.data['estado'],
            'fase': request.data['fase']
        }
        serializer = LineaBaseSerializer(data=lineabase)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LineaBaseDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = LineaBase.objects.all()
    serializer_class = LineaBaseSerializer


class LineaBaseEstadoList(generics.ListCreateAPIView):
    queryset = EstadoLineaBase.objects.all()
    serializer_class = EstadoLineaBaseSerializer


class LineaBaseEstadoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = EstadoLineaBase.objects.all()
    serializer_class = EstadoLineaBaseSerializer


class LineaBaseItemPut(APIView):
    def get_object(self, pk):
        """

        :param pk: clave primaria de línea base
        :return: un objeto línea base

        Esta función retorna un objeto de línea base
        """
        try:
            return LineaBase.objects.get(pk=pk)
        except LineaBase.DoesNotExist:
            raise Http404

    def put(self, request, pk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de línea base
        :param format:
        :return: una respuesta HTTP de éxito

        Esta función establece un estado para una línea base
        """

        lb_id = request.data.get('id')
        estado_cerrado = EstadoLineaBase.objects.get(nombre='CE')
        lb = LineaBase.objects.get(pk=lb_id)
        lb.estado = estado_cerrado
        lb.save()

        items = request.data.get('items')
        for it in items:
            item = Item.objects.get(pk=it["id"])
            item.lineaBase = lb
            item.save()
        return Response(status=status.HTTP_200_OK)

        # item = self.get_object(pk)
        # serializer = ItemSaveSerializer(item, data=request.data)
        # if serializer.is_valid():
        #     serializer.save()
        #     return Response(serializer.data)
        # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
