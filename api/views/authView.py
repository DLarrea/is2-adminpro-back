from rest_framework.decorators import api_view
from rest_framework.response import Response
import json
from django.contrib.auth.models import User
from rest_framework import status
from django.shortcuts import get_object_or_404
from api.models import RolProyectoPermiso, RolSistemaPermiso, RolProyecto, RolSistema, RolProyectoUser, RolSistemaUser, PermisoProyecto, PermisoSistema
from api.serializers.permisos.permisoSistemaSerializer import PermisoSistemaSerializer
from api.serializers.permisos.permisoProyectoSerializer import PermisoProyectoSerializer
from api.serializers.roles.rolSistemaSerializer import RolSistemaSerializer
from api.serializers.roles.rolProyectoSerializer import RolProyectoSerializer
from .userView import obtenerPermisosSistema
from api.models import RolSistemaUser, RolSistemaPermiso, PermisoSistema
from api.utils.ids import IdsManager
import pyrebase

config = {
    "apiKey": "AIzaSyDKJBlAy_TFarWFA4GsD-FC42ZqAzYHvuY",
    "authDomain": "is2-admin-pro.firebaseapp.com",
    "databaseURL": "https://is2-admin-pro.firebaseio.com",
    "projectId": "is2-admin-pro",
    "storageBucket": "is2-admin-pro.appspot.com",
    "messagingSenderId": "106217067862",
    "appId": "1:106217067862:web:71528e0bafb8e49b89f2f7"
}

firebase = pyrebase.initialize_app(config)
auth = firebase.auth()


@api_view(["POST"])
def login(request):
    """
    :param request: recibe una petición HTTP
    :return: un error o login exitoso

    Esta función loguea al usuario al sistema
    """
    data = json.loads(request.body.decode('utf-8'))

    email = data.get('username')
    password = data.get('password')

    try:
        is_user = get_object_or_404(User, username=email)
    except:
        response = {
            "error":True,
            "mensaje":"Usuario y/o contraseña inválido"
        }
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    try:
        user = auth.sign_in_with_email_and_password(email,password)
        
        perms = []
        roles_ids = RolSistemaUser.objects.filter(user_id=is_user.id).values('rol_id')
        roles_ids_raw = IdsManager.ids_raw_rol(roles_ids)
        print(roles_ids_raw)
        for id in roles_ids_raw:
            permisos_ids = RolSistemaPermiso.objects.filter(rol_id=id).values('permiso_id')
            permisos_ids_raw = IdsManager.ids_raw_permiso(permisos_ids)
            permisos = PermisoSistema.objects.filter(id__in=permisos_ids_raw)
            for p in permisos:
                isIn = False
                for r in perms:
                    if p.id == r["id"]:
                        isIn = True
                if not isIn:
                    perms.append({
                        "id": p.id,
                        "permiso": p.permiso,
                        "permiso_nombre": p.permiso_nombre
                    })

        response = {
            "currentUser": {
                "id": is_user.id,
                "username": is_user.username,
                "first_name": is_user.first_name,
                "last_name": is_user.last_name,
                "email": is_user.email,
                "ps": perms
            },
            "firebase": user
        }

        return Response(response, status=status.HTTP_201_CREATED)
    except:
        response = {
            "error": True,
            "mensaje": "Usuario y/o contraseña inválido"
        }
        return Response(response, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

@api_view(["POST"])
def is_auth(request):
    """

    :param request: recibe una petición HTTP
    :return: una respuesta HTTP 200

    Esta función verifica la autenticación de un usuario
    """
    data = json.loads(request.body)
    user = auth.refresh(data.get('refreshToken'))
    response = {
        "firebase":user
    }
    return Response(response, status=status.HTTP_200_OK)
