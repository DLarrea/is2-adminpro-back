from api.models import ProyectoRol, Proyecto, Rol
from api.serializers.proyectoRolSerializer import ProyectoRolSerializer
from api.serializers.proyectoSerializer import ProyectoSerializer
from api.serializers.rolSerializer import RolSerializer
from django.http import Http404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from django.shortcuts import get_object_or_404, get_list_or_404
from django.db import connection

import json

class ProyectoRolView(APIView):

    def get(self, request, pk, format=None):
        """

        :param request: recibe una peticion HTTP
        :param pk: clave foránea asociada a un rol
        :param format:
        :return: una respuesta HTTP de obtención del recurso "rol" exitoso

        Esta función obtiene los roles de proyecto
        """
        roles = Rol.objects.all()
        roles_proyecto_all = ProyectoRol.objects.all()
        roles_proyecto = roles_proyecto_all.filter(proyecto_id=pk)

        result = []

        for rol in roles:
            is_in = False
            for rol_asig in roles_proyecto:
                if rol.id == rol_asig.rol_id:
                    is_in = True
                    break
            if is_in:
                result.append(rol)

        s_roles = RolSerializer(result, many=True)
        return Response(s_roles.data, status=status.HTTP_200_OK)

    def post(self, request, pk, format=None):
        """

        :param request: es una petición HTTP
        :param pk: clave foránea asociada a un rol
        :param format:
        :return: una respuesta HTTP con valor de rol creado exitosamente o sin éxito
        """
        serializer = ProyectoRolSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ProyectoRolDeleteView(APIView):
    def delete(self, request, pk, rk, format=None):
        """

        :param request: recibe una petición HTTP
        :param pk: clave primaria de proyecto
        :param rk: clave primaria de rol
        :param format:
        :return: una respuesta HTTP
        Esta función elimina un rol de proyecto
        """
        rol_proyecto = ProyectoRol.objects.get(proyecto_id=pk, rol_id=rk)
        rol_proyecto.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

