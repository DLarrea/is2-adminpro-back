from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from api.views import userView
from api.views import rolSistemaView, proyectoView, lineaBaseView, rolProyectoView, notificacionView, relacionItemView, \
    atributosView, TipoItemAtributoView, faseView, tipoItemView, itemsView, estadoItemView, estadoProyectoView, comiteView, tipoItemFaseView, solicitudView

urlpatterns = [
    #Urls de roles de sistema
    path('roles-sistema/', rolSistemaView.getPostView.as_view()),
    path('roles-sistema/<int:pk>', rolSistemaView.getPutDeleteView.as_view()),
    path('roles-sistema/<int:pk>/permisos', rolSistemaView.obtenerPermisosView.as_view()),
    path('roles-sistema/<int:rk>/permisos/<int:pk>', rolSistemaView.asignarEliminarPermisoView.as_view()),

    # Urls de roles de proyectos
    path('roles-fase/', rolProyectoView.postRolFase.as_view()),
    path('roles-fase/<int:pk>/user/<int:uk>', rolProyectoView.putRolFase.as_view()),
    path('roles-proyecto/', rolProyectoView.postView.as_view()),
    path('proyecto/<int:pk>/roles-proyecto', rolProyectoView.getRolesProyectoView.as_view()),
    path('proyecto/<int:pk>/roles-proyecto/fases/<int:fk>', rolProyectoView.getRolesProyectoFaseView.as_view()),
    path('roles-proyecto/<int:pk>', rolProyectoView.getPutDeleteView.as_view()),
    path('roles-proyecto/<int:pk>/permisos', rolProyectoView.obtenerPermisosView.as_view()),
    path('roles-proyecto/<int:rk>/permisos/<int:pk>', rolProyectoView.asignarEliminarPermisoView.as_view()),

    #Urls de usuarios
    path('user/', userView.getPostView.as_view()),
    path('user/<int:pk>', userView.getPutDeleteView.as_view()),
    path('user/<int:uk>/roles-sistema', userView.getUserRolSistema.as_view()),
    path('user/<int:uk>/proyecto/<int:pk>/roles', userView.getUserRolProyecto.as_view()),
    path('user/<int:uk>/roles-sistema/<int:rk>', userView.asignarEliminarRolSistema.as_view()),
    path('user/<int:uk>/roles-proyecto/<int:rk>', userView.asignarEliminarRolProyecto.as_view()),
    path('user/<int:uk>/proyecto/<int:pk>/eliminar', userView.eliminarUsuarioProyecto.as_view()),
    path('user/<int:uk>/permisos-sistema', userView.obtenerPermisosSistema.as_view()),
    path('user/<int:uk>/permisos-proyecto/<int:pk>', userView.obtenerPermisosProyecto.as_view()),
    path('user/<int:uk>/permisos-proyecto/<int:pk>/fase/<int:fk>', userView.obtenerPermisosProyectoFase.as_view()),
    path('user/<int:uk>/roles-asignados-proyecto/<int:pk>', userView.rolesUsuario.as_view()),

    #Urls de proyectos
    path('proyecto/', proyectoView.getPostView.as_view()),
    path('proyecto/<int:pk>', proyectoView.getPutDeleteView.as_view()),
    path('proyecto/<int:pk>/iniciar', proyectoView.iniciarProyectoView.as_view()),
    path('proyecto/user/<int:pk>', proyectoView.getProyectosUserView.as_view()),
    path('proyecto/<int:pk>/users', proyectoView.getMiembrosProyectoView.as_view()),
    path('proyecto/user', proyectoView.postMiembroProyectoView.as_view()),
    path('proyecto/<int:pk>/fases', proyectoView.getFasesProyecto.as_view()),

    #urls de atributos
    path('atributo/', atributosView.AtributoList.as_view()),
    path('atributo/<int:pk>', atributosView.AtributoDetail.as_view()),

    # urls de Tipo de Item Atributos
    path('tipoItemAtributo/', TipoItemAtributoView.TipoItemAtributoList.as_view()),
    path('tipoItemAtributo/<int:pk>', TipoItemAtributoView.TipoItemAtributoDetail.as_view()),

    #urls para Fases
    path('fase/', faseView.faseListView.as_view()),
    path('fase/<int:pk>', faseView.faseDetailView.as_view()),
    path('fase/<int:pk>/iniciar', faseView.IniciarProyectoFase.as_view()),

    # urls de Tipo de Item
    path('tipoItem/proyecto/<int:pk>', tipoItemView.tipoItemListProyecto.as_view()),
    path('tipoItem/', tipoItemView.tipoItemList.as_view()),
    path('tipoItem/importar', tipoItemView.tipoItemImportar.as_view()),
    path('tipoItem/<int:pk>', tipoItemView.tipoItemDetail.as_view()),
    path('tipoItem/<int:pk>/gestionar', tipoItemView.tipoItemGestionar.as_view()),
    path('tipoItem/<int:pk>/atributos', tipoItemView.tipoItemAtributos.as_view()),
    path('tipoItem/fases/', tipoItemFaseView.tipoItemFaseList.as_view()),
    path('tipoItem/fases/<int:pk>', tipoItemFaseView.tipoItemFaseDetail.as_view()),

    #urls para items
    path('item/', itemsView.ItemList.as_view()),
    path('item/<int:pk>', itemsView.ItemDetail.as_view()),
    path('item/<int:pk>/historial', itemsView.ItemHistorial.as_view()),
    path('item-estado/', estadoItemView.ItemEstadoList.as_view()),
    path('item-estado/<int:pk>', estadoItemView.ItemEstadoDetail.as_view()),
    path('item-proyecto/<int:pk>', itemsView.ItemsProyectoList.as_view()),
    path('item-revision/<int:pk>', itemsView.RevisionItemsList.as_view()),
    path('item-impacto/<int:pk>', itemsView.ImpactoItemsList.as_view()),

    #urls para estados Proyecto
    path('estado-proyecto/', estadoProyectoView.EstadoProyectoList.as_view()),
    path('estado-proyecto/<int:pk>', estadoProyectoView.EstadoProyectoDetail.as_view()),

    #urls para comite
    path('comite/',  comiteView.ComiteList.as_view()),
    path('comite/<int:pk>', comiteView.ComiteDetail.as_view()),
    path('iscomite/<int:pk>', comiteView.IsComite.as_view()),

    #Urls de linea base
    path('linea-base/', lineaBaseView.LineaBaseList.as_view()),
    path('linea-base/<int:pk>', lineaBaseView.LineaBaseDetail.as_view()),

    #Urls de estados linea base
    path('linea-base/estados', lineaBaseView.LineaBaseEstadoList.as_view()),
    path('linea-base/estados/<int:pk>', lineaBaseView.LineaBaseEstadoDetail.as_view()),
    path('linea-base/items', itemsView.ItemLineaBaseGet.as_view()),
    path('linea-base/cerrar/<int:pk>', lineaBaseView.LineaBaseItemPut.as_view()),

    #Urls de notificar linea base
    path('solicitud/', solicitudView.solicitudList.as_view()),
    path('solicitud/<int:pk>', solicitudView.solicitudDetail.as_view()),

    #Urls de notificaciones
    path('notificacion/', notificacionView.notificacionList.as_view()),
    path('notificacion/<int:pk>', notificacionView.NotificacionDetail.as_view()),

    #Urls de relacion items
    path('relacionar-item/', relacionItemView.relacionItemListView.as_view()),
    path('relacionar-item/<int:pk>', relacionItemView.relacionItemDetail.as_view()),

]

urlpatterns = format_suffix_patterns(urlpatterns)

