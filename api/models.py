from django.db import models
from django.contrib.auth.models import User
from model_utils import Choices
from django.utils import timezone
# Create your models here.

class EstadoProyecto(models.Model): #
    nombre = models.CharField(max_length=50)

class Proyecto(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=1000)
    estadoProyecto = models.ForeignKey(EstadoProyecto, on_delete=models.CASCADE, related_name='estado')
    gerente = models.ForeignKey(User, on_delete=models.PROTECT, null=True, blank=True)
    observacion = models.CharField(max_length=1000, null=True, blank=True)
    fecha_inicio = models.CharField(max_length=20, null=True, blank=True)
    cantidad_comite = models.IntegerField(null=True, blank=True, default= 3)

class Fase(models.Model):
    types = Choices('CR', 'AB', 'CE')
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=1000)
    estado_fase = models.CharField(choices=types, max_length=2, default='CR')
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    orden = models.IntegerField(null=True, blank=True)

class PermisoSistema(models.Model):
    permiso = models.CharField(max_length=50)
    permiso_nombre = models.CharField(max_length=150)

class RolSistema(models.Model):
    nombre = models.CharField(max_length=100, unique=False)

class RolSistemaUser(models.Model):
    rol = models.ForeignKey(RolSistema, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

class RolSistemaPermiso(models.Model):
    rol = models.ForeignKey(RolSistema, on_delete=models.CASCADE)
    permiso = models.ForeignKey(PermisoSistema, on_delete=models.CASCADE)

class PermisoProyecto(models.Model):
    permiso = models.CharField(max_length=50)
    permiso_nombre = models.CharField(max_length=150)
    types = Choices('F', 'P')
    tipo_permiso = models.CharField(choices=types, max_length=1, default='P')

class RolProyecto(models.Model):
    nombre = models.CharField(max_length=100, unique=False)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE, null=True, blank=True)

class RolProyectoPermiso(models.Model):
    rol = models.ForeignKey(RolProyecto, on_delete=models.CASCADE)
    permiso = models.ForeignKey(PermisoProyecto, on_delete=models.CASCADE)


class RolProyectoUser(models.Model):
    rol = models.ForeignKey(RolProyecto, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.PROTECT)

class EstadoLineaBase(models.Model):
    nombre = models.CharField(max_length=50)

class LineaBase(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=1000)
    codigo = models.CharField(max_length=50, null=True, blank=True)
    observacion = models.CharField(max_length=1000, null=True, blank=True)
    estado = models.ForeignKey(EstadoLineaBase, on_delete=models.CASCADE)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE)

class Atributo(models.Model):
    types = Choices('T', 'N', 'A')
    nombre = models.CharField(max_length=50)
    type = models.CharField(choices=types, max_length=1, default='T')

class TipoItem(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=100)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE, null=True, blank=True)

class TipoItemFase(models.Model):
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE)
    tipoitem = models.ForeignKey(TipoItem, on_delete=models.PROTECT)
    class Meta:
        unique_together = ('fase', 'tipoitem',)

class TipoItemAtributo(models.Model):
    tipoitem = models.ForeignKey(TipoItem, on_delete=models.CASCADE)
    atributo = models.ForeignKey(Atributo, on_delete=models.PROTECT)


class EstadoItem(models.Model):
    nombre = models.CharField(max_length=50)

class Item(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.TextField()
    version = models.IntegerField()
    prioridad = models.IntegerField()
    lineaBase = models.ForeignKey(LineaBase, on_delete=models.PROTECT, null=True)
    estado = models.ForeignKey(EstadoItem, on_delete=models.PROTECT, related_name='estado')
    estado_ant = models.ForeignKey(EstadoItem, null=True, blank=True, on_delete=models.PROTECT, related_name='estado_ant')
    tipo = models.ForeignKey(TipoItemFase, on_delete=models.PROTECT)
    ant = models.ForeignKey("self", on_delete=models.CASCADE, null=True, related_name='anterior')
    sig = models.ForeignKey("self", on_delete=models.CASCADE, null=True, related_name='siguiete')
    current = models.BooleanField()
    values = models.TextField()

class RelacionItem(models.Model):
    tipos = Choices('PH', 'AS')
    item_relacion_1 = models.ForeignKey(Item, null=True, related_name='item_relacion_1', on_delete=models.CASCADE)
    item_relacion_2 = models.ForeignKey(Item, null=True, related_name='item_relacion_2', on_delete=models.CASCADE)
    tipo = models.CharField(choices=tipos, max_length=2, default='PH')
    class Meta:
        unique_together = ('item_relacion_1', 'item_relacion_2',)

class HistorialItem(models.Model):
    item_historial_1 = models.ForeignKey(Item, null=True, related_name='item_historial_1', on_delete=models.CASCADE)
    item_historial_2 = models.ForeignKey(Item, null=True, related_name='item_historial_2', on_delete=models.CASCADE)
    descripcion = models.CharField(max_length=1000)


class ComiteProyecto(models.Model):
    proyecto = models.ForeignKey(Proyecto, on_delete=models.PROTECT, null=False)
    miembro = models.ForeignKey(User, on_delete=models.PROTECT, null=False)
    activo = models.BooleanField(default=True)
    class Meta:
        unique_together = ('proyecto', 'miembro', 'activo')

class Solicitud(models.Model):
    nombre = models.CharField(max_length=200)
    mensaje = models.TextField() 
    usuario_creacion = models.ForeignKey(User, on_delete=models.CASCADE, null=False)
    lineaBase = models.ForeignKey(LineaBase, on_delete=models.CASCADE, null=True)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    estados = Choices('PE', 'SAP', 'SRE')
    estado = models.CharField(choices=estados, max_length=20, default='PE')
    fecha_creacion = models.DateTimeField(editable=False)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, null=True, blank=True)
    impacto = models.IntegerField(default=0)
    items_afectados = models.TextField(max_length=200,default='')

    def save(self, *args, **kwargs):
        if not self.id:
            self.fecha_creacion = timezone.now()
        return super(Solicitud, self).save(*args, **kwargs)

class SolicitudVotacion(models.Model):
    miembro = models.ForeignKey(ComiteProyecto, on_delete=models.PROTECT, null=False)
    voto = models.BooleanField(null=True, blank=True, default=None)
    solicitud = models.ForeignKey(Solicitud, on_delete=models.PROTECT, null=False)

class Notificacion(models.Model):
    mensaje = models.CharField(max_length=200)
    solicitud = models.ForeignKey(Solicitud, on_delete=models.PROTECT, null=False)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    fecha_creacion = models.DateTimeField(editable=False)

    def save(self, *args, **kwargs):
        if not self.id:
            self.fecha_creacion = timezone.now()
        return super(Notificacion, self).save(*args, **kwargs)