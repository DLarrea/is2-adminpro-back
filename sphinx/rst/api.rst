api package
===========

Subpackages
-----------

.. toctree::

   api.migrations

Submodules
----------

api.admin module
----------------

.. automodule:: api.admin
   :members:
   :undoc-members:
   :show-inheritance:

api.apps module
---------------

.. automodule:: api.apps
   :members:
   :undoc-members:
   :show-inheritance:

api.config module
-----------------

.. automodule:: api.config
   :members:
   :undoc-members:
   :show-inheritance:

api.models module
-----------------

.. automodule:: api.models
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: api
   :members:
   :undoc-members:
   :show-inheritance:
