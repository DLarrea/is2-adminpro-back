api.migrations package
======================

Submodules
----------

api.migrations.0001\_initial module
-----------------------------------

.. automodule:: api.migrations.0001_initial
   :members:
   :undoc-members:
   :show-inheritance:

api.migrations.0002\_rol\_type module
-------------------------------------

.. automodule:: api.migrations.0002_rol_type
   :members:
   :undoc-members:
   :show-inheritance:

api.migrations.0003\_rol\_users module
--------------------------------------

.. automodule:: api.migrations.0003_rol_users
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: api.migrations
   :members:
   :undoc-members:
   :show-inheritance:
