
------------------------------
--USUARIOS
INSERT INTO public.auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
VALUES ('pbkdf2_sha256$216000$Gi39ViFbq4qT$SDLlCr4Tn3Ihk5WviWX7Vus+jNytibYQDc/XonI9llk=', NULL, FALSE, 'gabrielarrea10@gmail.com', 'Diego', 
    'Larrea', 'gabrielarrea10@gmail.com', FALSE, TRUE, '2020-09-11 20:46:59');

INSERT INTO public.auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
VALUES ('pbkdf2_sha256$216000$F1GDexwYNAK0$fo+HgkXJsiXgNzxIS4mOm1WrVLZfnLEYYmmZTZ//4ME=', NULL, FALSE, 'test1@gmail.com', 'TEST', 
    'TEST', 'test1@gmail.com', FALSE, TRUE, '2020-09-11 20:46:59');

INSERT INTO public.auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
VALUES ('pbkdf2_sha256$216000$dEVmc9whSPiq$KCk0LatLtYsn3gsaOopZUR25NhBJt+M4t0Vv7ovZs4s=', NULL, FALSE, 'test2@gmail.com', 'TEST', 
    'TEST', 'test2@gmail.com', FALSE, TRUE, '2020-09-11 20:46:59');

INSERT INTO public.auth_user(password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined)
VALUES ('pbkdf2_sha256$216000$5H6nhgVlb5In$iLFJY7PqU+LsSYmgdRdhjo3/o+253J1qL/7OJvkG0CA=', NULL, FALSE, 'test3@gmail.com', 'TEST', 
    'TEST', 'test3@gmail.com', FALSE, TRUE, '2020-09-11 20:46:59');

------------------------------
--PERMISOS DE PROYECTOS
INSERT INTO public.api_permisoproyecto (permiso,permiso_nombre,tipo_permiso) VALUES 
('p_roles','Gestionar Roles del Proyecto','P')
,('p_usuarios','Gestionar Usuarios del Proyecto','P')
,('p_tipos_item','Gestionar Tipos de Item','P')
,('p_atributos','Gestionar Atributos','P')
,('p_fases','Gestionar Fases','P')
,('p_fases_tipos_item','Gestionar Fases Tipos de Item','P')
,('p_fases_usuarios','Gestionar Fases Usuarios','P')
,('p_comite','Gestionar Comité del Proyecto','P')
,('p_relaciones','Gestionar relaciones de ítems','P')
,('p_revisar','Revisar Items','P')
;
--Permisos por fase de Proyecto
INSERT INTO public.api_permisoproyecto (permiso,permiso_nombre,tipo_permiso) VALUES 
('i_desarrollar','Desarrollar Items','F')
,('i_s_aprobacion','Solicitar Aprobación Items','F')
,('i_aprobar','Aprobar Items','F')
,('i_cancelar','Cancelar Items','F')
,('i_linea_base','Gestionar Líneas Bases','F')
,('i_historial','Ver Historial','F')
,('i_impacto','Ver Impacto de modificación','F')
;

------------------------------
--PERMISOS DE SISTEMA
INSERT INTO public.api_permisosistema (permiso,permiso_nombre) VALUES 
('roles','Gestionar roles')
,('usuarios','Gestionar usuarios')
,('proyectos_add','Agregar proyectos')
;

------------------------------
--ROLES DEL SISTEMA
INSERT INTO public.api_rolsistema (nombre) VALUES 
('Admin')
,('Gestor de Roles')
,('Gestor de Usuarios')
;

------------------------------
--ESTADOS DE ITEMS
INSERT INTO public.api_estadoitem (nombre) VALUES 
('DE')
,('PP')
,('AP')
,('CA')
,('RE')
;

------------------------------
--ESTADOS LINEA BASE
INSERT INTO public.api_estadolineabase (nombre) VALUES 
('AB')
,('CE')
,('RO')
,('RE')
;

------------------------------
--ESTADOS PROYECTO
INSERT INTO public.api_estadoproyecto (nombre) VALUES 
('PENDIENTE')
,('EN DESARROLLO')
,('FINALIZADO')
,('CANCELADO')
;

------------------------------
--ATRIBUTOS
INSERT INTO public.api_atributo (nombre,"type") VALUES 
('Nombre','T')
,('Documento','A')
,('Apellidos','T')
,('Edad','N')
;

------------------------------
--ROL SISTEMA USER
INSERT INTO public.api_rolsistemauser (rol_id,user_id) VALUES 
((select id from public.api_rolsistema where nombre='Admin'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
,((select id from public.api_rolsistema where nombre='Gestor de Roles'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolsistema where nombre='Gestor de Usuarios'),(select id from public.auth_user where username='test2@gmail.com'))
;

------------------------------
--ROL SISTEMA PERMISO
INSERT INTO public.api_rolsistemapermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisosistema where permiso='roles'),(select id from public.api_rolsistema where nombre='Admin'))
,((select id from public.api_permisosistema where permiso='usuarios'),(select id from public.api_rolsistema where nombre='Admin'))
,((select id from public.api_permisosistema where permiso='proyectos_add'),(select id from public.api_rolsistema where nombre='Admin'))
,((select id from public.api_permisosistema where permiso='roles'),(select id from public.api_rolsistema where nombre='Gestor de Roles'))
,((select id from public.api_permisosistema where permiso='usuarios'),(select id from public.api_rolsistema where nombre='Gestor de Usuarios'))
;

------------------------------
--PROYECTOS
INSERT INTO public.api_proyecto (nombre,descripcion,"estadoProyecto_id",fecha_inicio,gerente_id,observacion,cantidad_comite) VALUES 
('Proyecto 1','Descripcion Proyecto 1', (select id from public.api_estadoproyecto where nombre='EN DESARROLLO') ,'12/09/2020',NULL,'Proyecto 1',3);
INSERT INTO public.api_proyecto (nombre,descripcion,"estadoProyecto_id",fecha_inicio,gerente_id,observacion,cantidad_comite) VALUES 
('Proyecto 2','Descripcion Proyecto 2', (select id from public.api_estadoproyecto where nombre='EN DESARROLLO') ,'12/09/2020',NULL,'Proyecto 2',3);
INSERT INTO public.api_proyecto (nombre,descripcion,"estadoProyecto_id",fecha_inicio,gerente_id,observacion,cantidad_comite) VALUES 
('Proyecto 3','Descripcion Proyecto 3', (select id from public.api_estadoproyecto where nombre='EN DESARROLLO') ,'12/09/2020',NULL,'Proyecto 3',3);

------------------------------
--FASES
--Proyecto 1
INSERT INTO public.api_fase (nombre,descripcion,proyecto_id,estado_fase,orden) VALUES 
('Fase 1 Proyecto 1','Items Fase 1',(select id from public.api_proyecto where nombre='Proyecto 1'),'AB',1)
,('Fase 2 Proyecto 1','Items Fase 2',(select id from public.api_proyecto where nombre='Proyecto 1'),'AB',2)
,('Fase 3 Proyecto 1','Items Fase 3',(select id from public.api_proyecto where nombre='Proyecto 1'),'AB',3)
;
--Proyecto 2
INSERT INTO public.api_fase (nombre,descripcion,proyecto_id,estado_fase,orden) VALUES 
('Fase 1 Proyecto 2','Items Fase 1',(select id from public.api_proyecto where nombre='Proyecto 2'),'AB',1)
,('Fase 2 Proyecto 2','Items Fase 2',(select id from public.api_proyecto where nombre='Proyecto 2'),'AB',2)
,('Fase 3 Proyecto 2','Items Fase 3',(select id from public.api_proyecto where nombre='Proyecto 2'),'AB',3)
;
--Proyecto 3
INSERT INTO public.api_fase (nombre,descripcion,proyecto_id,estado_fase,orden) VALUES 
('Fase 1 Proyecto 3','Items Fase 1',(select id from public.api_proyecto where nombre='Proyecto 3'),'AB',1)
,('Fase 2 Proyecto 3','Items Fase 2',(select id from public.api_proyecto where nombre='Proyecto 3'),'AB',2)
,('Fase 3 Proyecto 3','Items Fase 3',(select id from public.api_proyecto where nombre='Proyecto 3'),'AB',3)
;

------------------------------
--ROLES DE PROYECTO
--Proyecto 1
INSERT INTO public.api_rolproyecto (nombre,proyecto_id,fase_id) VALUES 
('P1_Gerente',(select id from public.api_proyecto where nombre='Proyecto 1'),NULL)
,('P1_Gestor de Roles',(select id from public.api_proyecto where nombre='Proyecto 1'),NULL)
,('P1_Visitante',(select id from public.api_proyecto where nombre='Proyecto 1'),NULL)
,('P1_Encargado_Revision_Items',(select id from public.api_proyecto where nombre='Proyecto 1'),NULL)
,('P1F1_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 1 Proyecto 1'))
,('P1F1_QA',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 1 Proyecto 1'))
,('P1F1_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 1 Proyecto 1'))
,('P1F2_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 2 Proyecto 1'))
,('P1F2_QA',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 2 Proyecto 1'))
,('P1F2_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 2 Proyecto 1'))
,('P1F3_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 3 Proyecto 1'))
,('P1F3_QA',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 3 Proyecto 1'))
,('P1F3_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 1'),(select id from public.api_fase where nombre='Fase 3 Proyecto 1'))
;
--Proyecto 2
INSERT INTO public.api_rolproyecto (nombre,proyecto_id,fase_id) VALUES 
('P2_Gerente',(select id from public.api_proyecto where nombre='Proyecto 2'),NULL)
,('P2_Gestor de Roles',(select id from public.api_proyecto where nombre='Proyecto 2'),NULL)
,('P2_Visitante',(select id from public.api_proyecto where nombre='Proyecto 2'),NULL)
,('P2_Encargado_Revision_Items',(select id from public.api_proyecto where nombre='Proyecto 2'),NULL)
,('P2F1_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 1 Proyecto 2'))
,('P2F1_QA',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 1 Proyecto 2'))
,('P2F1_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 1 Proyecto 2'))
,('P2F2_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 2 Proyecto 2'))
,('P2F2_QA',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 2 Proyecto 2'))
,('P2F2_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 2 Proyecto 2'))
,('P2F3_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 3 Proyecto 2'))
,('P2F3_QA',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 3 Proyecto 2'))
,('P2F3_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 2'),(select id from public.api_fase where nombre='Fase 3 Proyecto 2'))
;
--Proyecto 3
INSERT INTO public.api_rolproyecto (nombre,proyecto_id,fase_id) VALUES 
('P3_Gerente',(select id from public.api_proyecto where nombre='Proyecto 3'),NULL)
,('P3_Gestor de Roles',(select id from public.api_proyecto where nombre='Proyecto 3'),NULL)
,('P3_Visitante',(select id from public.api_proyecto where nombre='Proyecto 3'),NULL)
,('P3_Encargado_Revision_Items',(select id from public.api_proyecto where nombre='Proyecto 3'),NULL)
,('P3F1_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 1 Proyecto 3'))
,('P3F1_QA',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 1 Proyecto 3'))
,('P3F1_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 1 Proyecto 3'))
,('P3F2_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 2 Proyecto 3'))
,('P3F2_QA',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 2 Proyecto 3'))
,('P3F2_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 2 Proyecto 3'))
,('P3F3_Desarrollador',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 3 Proyecto 3'))
,('P3F3_QA',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 3 Proyecto 3'))
,('P3F3_Admin_Fase',(select id from public.api_proyecto where nombre='Proyecto 3'),(select id from public.api_fase where nombre='Fase 3 Proyecto 3'))
;

------------------------------
--PERMISOS ROLES DE PROYECTO
----Proyecto 1----
--Rol Gerente
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_roles'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_usuarios'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases_usuarios'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_comite'),(select id from public.api_rolproyecto where nombre='P1_Gerente'))
;
--Rol Gestor de Roles
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_roles'),(select id from public.api_rolproyecto where nombre='P1_Gestor de Roles'))
,((select id from public.api_permisoproyecto where permiso='p_usuarios'),(select id from public.api_rolproyecto where nombre='P1_Gestor de Roles'))
,((select id from public.api_permisoproyecto where permiso='p_comite'),(select id from public.api_rolproyecto where nombre='P1_Gestor de Roles'))
;
--Rol Desarrollador
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'))

,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'))

,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'))
;
--Rol QA
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P1F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F1_QA'))

,((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P1F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F2_QA'))

,((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P1F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F3_QA'))
;
--Rol Admin Fase
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'))

,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'))

,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'))
;
--Rol Encargado de Revision de Items 
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_revisar'),(select id from public.api_rolproyecto where nombre='P1_Encargado_Revision_Items'))
;


----Proyecto 2----
--Rol Gerente
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_roles'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_usuarios'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases_usuarios'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_comite'),(select id from public.api_rolproyecto where nombre='P2_Gerente'))
;
--Rol Gestor de Roles
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_roles'),(select id from public.api_rolproyecto where nombre='P2_Gestor de Roles'))
,((select id from public.api_permisoproyecto where permiso='p_usuarios'),(select id from public.api_rolproyecto where nombre='P2_Gestor de Roles'))
,((select id from public.api_permisoproyecto where permiso='p_comite'),(select id from public.api_rolproyecto where nombre='P2_Gestor de Roles'))
;
--Rol Desarrollador
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'))

,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'))

,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'))
;
--Rol QA
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P2F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F1_QA'))

,((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P2F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F2_QA'))

,((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P2F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F3_QA'))
;
--Rol Admin Fase
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'))

,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'))

,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'))
;
--Rol Encargado de Revision de Items 
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_revisar'),(select id from public.api_rolproyecto where nombre='P2_Encargado_Revision_Items'))
;


----Proyecto 3----
--Rol Gerente
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_roles'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_usuarios'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_fases_usuarios'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
,((select id from public.api_permisoproyecto where permiso='p_comite'),(select id from public.api_rolproyecto where nombre='P3_Gerente'))
;
--Rol Gestor de Roles
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_roles'),(select id from public.api_rolproyecto where nombre='P3_Gestor de Roles'))
,((select id from public.api_permisoproyecto where permiso='p_usuarios'),(select id from public.api_rolproyecto where nombre='P3_Gestor de Roles'))
,((select id from public.api_permisoproyecto where permiso='p_comite'),(select id from public.api_rolproyecto where nombre='P3_Gestor de Roles'))
;
--Rol Desarrollador
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'))

,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'))

,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_s_aprobacion'),(select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'))
,((select id from public.api_permisoproyecto where permiso='p_relaciones'),(select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'))
;
--Rol QA
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P3F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F1_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F1_QA'))

,((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P3F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F2_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F2_QA'))

,((select id from public.api_permisoproyecto where permiso='i_aprobar'),(select id from public.api_rolproyecto where nombre='P3F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F3_QA'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F3_QA'))
;
--Rol Admin Fase
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'))

,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'))

,((select id from public.api_permisoproyecto where permiso='i_cancelar'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_desarrollar'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_historial'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_linea_base'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_fases_tipos_item'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_tipos_item'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='p_atributos'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
,((select id from public.api_permisoproyecto where permiso='i_impacto'),(select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'))
;
--Rol Encargado de Revision de Items 
INSERT INTO public.api_rolproyectopermiso (permiso_id,rol_id) VALUES 
((select id from public.api_permisoproyecto where permiso='p_revisar'),(select id from public.api_rolproyecto where nombre='P3_Encargado_Revision_Items'))
;

------------------------------
--ROLES DE PROYECTO USER
----Proyecto 1----
INSERT INTO public.api_rolproyectouser (rol_id, user_id) VALUES
((select id from public.api_rolproyecto where nombre='P1_Gerente'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1_Encargado_Revision_Items'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1_Visitante'),(select id from public.auth_user where username='test3@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1F1_Desarrollador'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1F1_QA'),(select id from public.auth_user where username='test2@gmail.com'))

,((select id from public.api_rolproyecto where nombre='P1F1_Admin_Fase'),(select id from public.auth_user where username='test2@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1F2_Desarrollador'),(select id from public.auth_user where username='test2@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1F2_QA'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1F2_Admin_Fase'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))

,((select id from public.api_rolproyecto where nombre='P1F3_Desarrollador'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1F3_QA'),(select id from public.auth_user where username='test2@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P1F3_Admin_Fase'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
;
----Proyecto 2----
INSERT INTO public.api_rolproyectouser (rol_id, user_id) VALUES
((select id from public.api_rolproyecto where nombre='P2_Gerente'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2_Encargado_Revision_Items'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2_Visitante'),(select id from public.auth_user where username='test2@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2F1_Desarrollador'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2F1_QA'),(select id from public.auth_user where username='test3@gmail.com'))

,((select id from public.api_rolproyecto where nombre='P2F1_Admin_Fase'),(select id from public.auth_user where username='test3@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2F2_Desarrollador'),(select id from public.auth_user where username='test3@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2F2_QA'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2F2_Admin_Fase'),(select id from public.auth_user where username='test1@gmail.com'))

,((select id from public.api_rolproyecto where nombre='P2F3_Desarrollador'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2F3_QA'),(select id from public.auth_user where username='test3@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P2F3_Admin_Fase'),(select id from public.auth_user where username='test1@gmail.com'))
;

----Proyecto 3----
INSERT INTO public.api_rolproyectouser (rol_id, user_id) VALUES
((select id from public.api_rolproyecto where nombre='P3_Gerente'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3_Encargado_Revision_Items'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3_Visitante'),(select id from public.auth_user where username='test3@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3F1_Desarrollador'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3F1_QA'),(select id from public.auth_user where username='test2@gmail.com'))

,((select id from public.api_rolproyecto where nombre='P3F1_Admin_Fase'),(select id from public.auth_user where username='test2@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3F2_Desarrollador'),(select id from public.auth_user where username='test2@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3F2_QA'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3F2_Admin_Fase'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))

,((select id from public.api_rolproyecto where nombre='P3F3_Desarrollador'),(select id from public.auth_user where username='test1@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3F3_QA'),(select id from public.auth_user where username='test2@gmail.com'))
,((select id from public.api_rolproyecto where nombre='P3F3_Admin_Fase'),(select id from public.auth_user where username='gabrielarrea10@gmail.com'))
;

------------------------------
--LINEAS BASE
INSERT INTO public.api_lineabase (nombre,descripcion,codigo,observacion,estado_id,fase_id) VALUES 
('P1F1_LineaBase1','Fase 1 Proyecto 1','LB_1-1','1 Item',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 1 Proyecto 1'))
,('P1F1_LineaBase2','Fase 1 Proyecto 1','LB_1-2','2 Items',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 1 Proyecto 1'))
,('P1F2_LineaBase1','Fase 2 Proyecto 1','LB_2-1','2 Items',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 2 Proyecto 1'))
;
INSERT INTO public.api_lineabase (nombre,descripcion,codigo,observacion,estado_id,fase_id) VALUES 
('P2F1_LineaBase1','Fase 1 Proyecto 2','LB_1-1','1 Item',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 1 Proyecto 2'))
,('P2F1_LineaBase2','Fase 1 Proyecto 2','LB_1-2','2 Items',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 1 Proyecto 2'))
,('P2F2_LineaBase1','Fase 2 Proyecto 2','LB_2-1','2 Items',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 2 Proyecto 2'))
,('P2F2_LineaBase2','Fase 2 Proyecto 2','LB_2-1','2 Items',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 2 Proyecto 2'))
;
INSERT INTO public.api_lineabase (nombre,descripcion,codigo,observacion,estado_id,fase_id) VALUES 
('P3F1_LineaBase1','Fase 1 Proyecto 3','LB_1-1','1 Item',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 1 Proyecto 3'))
,('P3F1_LineaBase2','Fase 1 Proyecto 3','LB_1-2','2 Items',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 1 Proyecto 3'))
,('P3F2_LineaBase1','Fase 2 Proyecto 3','LB_2-1','2 Items',(select id from public.api_estadolineabase where nombre='CE'),(select id from public.api_fase where nombre='Fase 2 Proyecto 3'))
;

------------------------------
--TIPOS ITEM
INSERT INTO public.api_tipoitem (nombre,descripcion,proyecto_id) VALUES 
('P1_TipoItem1','Proyecto 1',(select id from public.api_proyecto where nombre='Proyecto 1'))
,('P1_TipoItem2','Proyecto 1',(select id from public.api_proyecto where nombre='Proyecto 1'))
,('P1_TipoItem3','Proyecto 1',(select id from public.api_proyecto where nombre='Proyecto 1'))
;
INSERT INTO public.api_tipoitem (nombre,descripcion,proyecto_id) VALUES 
('P2_TipoItem1','Proyecto 2',(select id from public.api_proyecto where nombre='Proyecto 2'))
,('P2_TipoItem2','Proyecto 2',(select id from public.api_proyecto where nombre='Proyecto 2'))
,('P2_TipoItem3','Proyecto 2',(select id from public.api_proyecto where nombre='Proyecto 2'))
;
INSERT INTO public.api_tipoitem (nombre,descripcion,proyecto_id) VALUES 
('P3_TipoItem1','Proyecto 3',(select id from public.api_proyecto where nombre='Proyecto 3'))
,('P3_TipoItem2','Proyecto 3',(select id from public.api_proyecto where nombre='Proyecto 3'))
,('P3_TipoItem3','Proyecto 3',(select id from public.api_proyecto where nombre='Proyecto 3'))
;

------------------------------
--ATRIBUTOS TIPOS ITEM
INSERT INTO public.api_tipoitematributo (atributo_id,tipoitem_id) VALUES 
((select id from public.api_atributo where nombre='Nombre'),(select id from public.api_tipoitem where nombre='P1_TipoItem1'))
,((select id from public.api_atributo where nombre='Apellidos'),(select id from public.api_tipoitem where nombre='P1_TipoItem1'))
,((select id from public.api_atributo where nombre='Documento'),(select id from public.api_tipoitem where nombre='P1_TipoItem2'))
,((select id from public.api_atributo where nombre='Edad'),(select id from public.api_tipoitem where nombre='P1_TipoItem3'))
;
INSERT INTO public.api_tipoitematributo (atributo_id,tipoitem_id) VALUES 
((select id from public.api_atributo where nombre='Nombre'),(select id from public.api_tipoitem where nombre='P2_TipoItem1'))
,((select id from public.api_atributo where nombre='Apellidos'),(select id from public.api_tipoitem where nombre='P2_TipoItem1'))
,((select id from public.api_atributo where nombre='Documento'),(select id from public.api_tipoitem where nombre='P2_TipoItem2'))
,((select id from public.api_atributo where nombre='Edad'),(select id from public.api_tipoitem where nombre='P2_TipoItem3'))
;
INSERT INTO public.api_tipoitematributo (atributo_id,tipoitem_id) VALUES 
((select id from public.api_atributo where nombre='Nombre'),(select id from public.api_tipoitem where nombre='P3_TipoItem1'))
,((select id from public.api_atributo where nombre='Apellidos'),(select id from public.api_tipoitem where nombre='P3_TipoItem1'))
,((select id from public.api_atributo where nombre='Documento'),(select id from public.api_tipoitem where nombre='P3_TipoItem2'))
,((select id from public.api_atributo where nombre='Edad'),(select id from public.api_tipoitem where nombre='P3_TipoItem3'))
;

------------------------------
--TIPOS ITEM FASE
INSERT INTO public.api_tipoitemfase (fase_id, tipoitem_id) VALUES
((select id from public.api_fase where nombre='Fase 1 Proyecto 1'),(select id from public.api_tipoitem where nombre='P1_TipoItem1'))
,((select id from public.api_fase where nombre='Fase 2 Proyecto 1'),(select id from public.api_tipoitem where nombre='P1_TipoItem2'))
,((select id from public.api_fase where nombre='Fase 3 Proyecto 1'),(select id from public.api_tipoitem where nombre='P1_TipoItem3'))
;
INSERT INTO public.api_tipoitemfase (fase_id, tipoitem_id) VALUES
((select id from public.api_fase where nombre='Fase 1 Proyecto 2'),(select id from public.api_tipoitem where nombre='P2_TipoItem1'))
,((select id from public.api_fase where nombre='Fase 2 Proyecto 2'),(select id from public.api_tipoitem where nombre='P2_TipoItem2'))
,((select id from public.api_fase where nombre='Fase 3 Proyecto 2'),(select id from public.api_tipoitem where nombre='P2_TipoItem3'))
;
INSERT INTO public.api_tipoitemfase (fase_id, tipoitem_id) VALUES
((select id from public.api_fase where nombre='Fase 1 Proyecto 3'),(select id from public.api_tipoitem where nombre='P3_TipoItem1'))
,((select id from public.api_fase where nombre='Fase 2 Proyecto 3'),(select id from public.api_tipoitem where nombre='P3_TipoItem2'))
,((select id from public.api_fase where nombre='Fase 3 Proyecto 3'),(select id from public.api_tipoitem where nombre='P3_TipoItem3'))
;

------------------------------
--ITEMS
INSERT INTO public.api_item (nombre,descripcion,"version",prioridad,estado_id,"lineaBase_id",tipo_id,ant_id,"current",sig_id,"values",estado_ant_id) VALUES 
('P1F1_Item1','Item Aprobado en LB',1,10,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P1F1_LineaBase1'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)
,('P1F1_Item2','Item Aprobado en LB',1,3,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P1F1_LineaBase2'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)
,('P1F1_Item3','Item Aprobado en LB',1,6,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P1F1_LineaBase2'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)

,('P1F2_Item1','Item en Desarrollo',1,7,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 2 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem2')),NULL,true,NULL,'[{"value":"","type":"A"}]',NULL)
,('P1F2_Item2','Item Aprobado en LB',1,5,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P1F2_LineaBase1'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 2 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem2')),NULL,true,NULL,'[{"value":"","type":"A"}]',NULL)

,('P1F3_Item1','Item en Desarrollo',1,5,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem3')),NULL,true,NULL,'[{"value":10,"type":"N"}]',NULL)
,('P1F3_Item2','Item Aprobado',1,8,(select id from public.api_estadoitem where nombre='AP'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem3')),NULL,true,NULL,'[{"value":34,"type":"N"}]',NULL)
,('P1F3_Item3','Item en Desarrollo',1,4,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 1') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P1_TipoItem3')),NULL,true,NULL,'[{"value":52,"type":"N"}]',NULL)
;
INSERT INTO public.api_item (nombre,descripcion,"version",prioridad,estado_id,"lineaBase_id",tipo_id,ant_id,"current",sig_id,"values",estado_ant_id) VALUES 
('P2F1_Item1','Item Aprobado en LB',1,10,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P2F1_LineaBase1'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)
,('P2F1_Item2','Item Aprobado en LB',1,3,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P2F1_LineaBase2'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)
,('P2F1_Item3','Item Aprobado en LB',1,6,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P2F1_LineaBase2'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)

,('P2F2_Item1','Item en Desarrollo',1,7,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 2 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem2')),NULL,true,NULL,'[{"value":"","type":"A"}]',NULL)
,('P2F2_Item2','Item Aprobado en LB',1,5,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P2F2_LineaBase1'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 2 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem2')),NULL,true,NULL,'[{"value":"","type":"A"}]',NULL)

,('P2F3_Item1','Item en Desarrollo',1,5,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem3')),NULL,true,NULL,'[{"value":10,"type":"N"}]',NULL)
,('P2F3_Item2','Item Aprobado',1,8,(select id from public.api_estadoitem where nombre='AP'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem3')),NULL,true,NULL,'[{"value":34,"type":"N"}]',NULL)
,('P2F3_Item3','Item en Desarrollo',1,4,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 2') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P2_TipoItem3')),NULL,true,NULL,'[{"value":52,"type":"N"}]',NULL)
;
INSERT INTO public.api_item (nombre,descripcion,"version",prioridad,estado_id,"lineaBase_id",tipo_id,ant_id,"current",sig_id,"values",estado_ant_id) VALUES 
('P3F1_Item1','Item Aprobado en LB',1,10,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P3F1_LineaBase1'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)
,('P3F1_Item2','Item Aprobado en LB',1,3,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P3F1_LineaBase2'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)
,('P3F1_Item3','Item Aprobado en LB',1,6,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P3F1_LineaBase2'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 1 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem1')),NULL,true,NULL,'[{"value":"Nombres","type":"T"},{"value":"Apellidos","type":"T"}]',NULL)

,('P3F2_Item1','Item en Desarrollo',1,7,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 2 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem2')),NULL,true,NULL,'[{"value":"","type":"A"}]',NULL)
,('P3F2_Item2','Item Aprobado en LB',1,5,(select id from public.api_estadoitem where nombre='AP'),(select id from public.api_lineabase where nombre='P3F2_LineaBase1'),(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 2 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem2')),NULL,true,NULL,'[{"value":"","type":"A"}]',NULL)

,('P3F3_Item1','Item en Desarrollo',1,5,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem3')),NULL,true,NULL,'[{"value":10,"type":"N"}]',NULL)
,('P3F3_Item2','Item Aprobado',1,8,(select id from public.api_estadoitem where nombre='AP'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem3')),NULL,true,NULL,'[{"value":34,"type":"N"}]',NULL)
,('P3F3_Item3','Item en Desarrollo',1,4,(select id from public.api_estadoitem where nombre='DE'),NULL,(select id from public.api_tipoitemfase where fase_id=(select id from public.api_fase where nombre='Fase 3 Proyecto 3') AND tipoitem_id=(select id from public.api_tipoitem where nombre='P3_TipoItem3')),NULL,true,NULL,'[{"value":52,"type":"N"}]',NULL)
;

------------------------------
--COMITE
INSERT INTO public.api_comiteproyecto (miembro_id,proyecto_id,activo) VALUES 
((select id from public.auth_user where username='gabrielarrea10@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 1'),true)
,((select id from public.auth_user where username='test1@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 1'),true)
,((select id from public.auth_user where username='test2@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 1'),true)
;
INSERT INTO public.api_comiteproyecto (miembro_id,proyecto_id,activo) VALUES 
((select id from public.auth_user where username='gabrielarrea10@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 2'),true)
,((select id from public.auth_user where username='test1@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 2'),true)
,((select id from public.auth_user where username='test3@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 2'),true)
;
INSERT INTO public.api_comiteproyecto (miembro_id,proyecto_id,activo) VALUES 
((select id from public.auth_user where username='gabrielarrea10@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 3'),true)
,((select id from public.auth_user where username='test1@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 3'),true)
,((select id from public.auth_user where username='test2@gmail.com'),(select id from public.api_proyecto where nombre='Proyecto 3'),true)
;

------------------------------
--RELACIONES
INSERT INTO public.api_relacionitem (item_relacion_1_id,item_relacion_2_id,tipo) VALUES 
((select id from public.api_item where nombre='P1F1_Item1'),(select id from public.api_item where nombre='P1F1_Item2'),'PH')
,((select id from public.api_item where nombre='P1F1_Item1'),(select id from public.api_item where nombre='P1F1_Item3'),'PH')
,((select id from public.api_item where nombre='P1F1_Item3'),(select id from public.api_item where nombre='P1F2_Item1'),'AS')
,((select id from public.api_item where nombre='P1F1_Item3'),(select id from public.api_item where nombre='P1F2_Item2'),'AS')
,((select id from public.api_item where nombre='P1F2_Item2'),(select id from public.api_item where nombre='P1F3_Item1'),'AS')
,((select id from public.api_item where nombre='P1F2_Item2'),(select id from public.api_item where nombre='P1F3_Item2'),'AS')
;
INSERT INTO public.api_relacionitem (item_relacion_1_id,item_relacion_2_id,tipo) VALUES 
((select id from public.api_item where nombre='P2F1_Item1'),(select id from public.api_item where nombre='P2F1_Item2'),'PH')
,((select id from public.api_item where nombre='P2F1_Item3'),(select id from public.api_item where nombre='P2F2_Item1'),'AS')
,((select id from public.api_item where nombre='P2F2_Item1'),(select id from public.api_item where nombre='P2F3_Item1'),'AS')
,((select id from public.api_item where nombre='P2F2_Item1'),(select id from public.api_item where nombre='P2F3_Item2'),'AS')
;
INSERT INTO public.api_relacionitem (item_relacion_1_id,item_relacion_2_id,tipo) VALUES 
((select id from public.api_item where nombre='P3F1_Item1'),(select id from public.api_item where nombre='P3F1_Item2'),'PH')
,((select id from public.api_item where nombre='P3F1_Item3'),(select id from public.api_item where nombre='P3F2_Item1'),'AS')
,((select id from public.api_item where nombre='P3F1_Item3'),(select id from public.api_item where nombre='P3F2_Item2'),'AS')
,((select id from public.api_item where nombre='P3F2_Item1'),(select id from public.api_item where nombre='P3F3_Item1'),'AS')
;